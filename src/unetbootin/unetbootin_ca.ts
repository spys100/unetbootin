<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ca_ES">
<context>
    <name>QObject</name>
    <message>
        <location filename="main.cpp" line="266"/>
        <source>LeftToRight</source>
        <translation>D&apos;esquerra a dreta</translation>
    </message>
</context>
<context>
    <name>customver</name>
    <message>
        <location filename="customdistselect.cpp" line="10"/>
        <location filename="customdistrolst.cpp" line="10"/>
        <source>iscustomver-no</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="12"/>
        <location filename="customdistselect.cpp" line="13"/>
        <source>Custom UNetbootin Build</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="15"/>
        <location filename="customdistrolst.cpp" line="26"/>
        <source>Distro Name 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="15"/>
        <source>default-distro1version-1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="16"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.distro1site.org&quot;&gt;http://www.distro1site.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; This distro 1 is unique and great in these ways.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; You will have to do this to install this distribution 1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>distro1version-1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-1_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-1_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-1_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-2_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-2_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-2_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-3_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-3_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-3_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-4_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-4_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-4_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-5_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-5_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-5_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-6_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-6_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-6_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-7_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-7_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-7_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-8_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-8_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-8_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-9_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-9_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-9_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="20"/>
        <location filename="customdistselect.cpp" line="22"/>
        <location filename="customdistrolst.cpp" line="39"/>
        <source>Distro Name 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="22"/>
        <source>default-distro2version-1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="23"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.distro2site.org&quot;&gt;http://www.distro2site.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; This distro 2 is unique and great in these ways.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; You will have to do this to install this distribution 2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>distro2version-1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-1_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-1_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-1_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-2_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-2_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-2_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-3_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-3_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-3_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-4_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-4_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-4_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-5_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-5_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-5_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-6_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-6_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-6_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-7_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-7_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-7_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-8_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-8_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-8_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-9_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-9_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-9_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="28"/>
        <location filename="customdistselect.cpp" line="30"/>
        <location filename="customdistrolst.cpp" line="52"/>
        <source>Distro Name 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="30"/>
        <source>default-distro3version-1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="31"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.distro3site.org&quot;&gt;http://www.distro3site.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; This distro 3 is unique and great in these ways.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; You will have to do this to install this distribution 3.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>distro3version-1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-1_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-1_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-1_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-2_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-2_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-2_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-3_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-3_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-3_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-4_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-4_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-4_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-5_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-5_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-5_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-6_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-6_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-6_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-7_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-7_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-7_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-8_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-8_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-8_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-9_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-9_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-9_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="36"/>
        <location filename="customdistselect.cpp" line="38"/>
        <location filename="customdistrolst.cpp" line="65"/>
        <source>Distro Name 4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="38"/>
        <source>default-distro4version-1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="39"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.distro4site.org&quot;&gt;http://www.distro4site.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; This distro 4 is unique and great in these ways.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; You will have to do this to install this distribution 4.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>distro4version-1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-1_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-1_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-1_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-2_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-2_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-2_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-3_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-3_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-3_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-4_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-4_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-4_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-5_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-5_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-5_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-6_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-6_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-6_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-7_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-7_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-7_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-8_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-8_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-8_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-9_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-9_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-9_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="44"/>
        <location filename="customdistselect.cpp" line="46"/>
        <location filename="customdistrolst.cpp" line="78"/>
        <source>Distro Name 5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="46"/>
        <source>default-distro5version-1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="47"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.distro5site.org&quot;&gt;http://www.distro5site.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; This distro 5 is unique and great in these ways.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; You will have to do this to install this distribution 5.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>distro5version-1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-1_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-1_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-1_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-2_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-2_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-2_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-3_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-3_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-3_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-4_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-4_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-4_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-5_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-5_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-5_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-6_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-6_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-6_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-7_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-7_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-7_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-8_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-8_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-8_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-9_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-9_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-9_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="52"/>
        <location filename="customdistselect.cpp" line="54"/>
        <location filename="customdistrolst.cpp" line="91"/>
        <source>Distro Name 6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="54"/>
        <source>default-distro6version-1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="55"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.distro6site.org&quot;&gt;http://www.distro6site.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; This distro 6 is unique and great in these ways.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; You will have to do this to install this distribution 6.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>distro6version-1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-1_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-1_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-1_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-2_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-2_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-2_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-3_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-3_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-3_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-4_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-4_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-4_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-5_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-5_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-5_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-6_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-6_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-6_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-7_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-7_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-7_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-8_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-8_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-8_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-9_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-9_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-9_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="60"/>
        <location filename="customdistselect.cpp" line="62"/>
        <location filename="customdistrolst.cpp" line="104"/>
        <source>Distro Name 7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="62"/>
        <source>default-distro7version-1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="63"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.distro7site.org&quot;&gt;http://www.distro7site.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; This distro 7 is unique and great in these ways.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; You will have to do this to install this distribution 7.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>distro7version-1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-1_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-1_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-1_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-2_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-2_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-2_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-3_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-3_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-3_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-4_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-4_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-4_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-5_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-5_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-5_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-6_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-6_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-6_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-7_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-7_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-7_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-8_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-8_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-8_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-9_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-9_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-9_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="68"/>
        <location filename="customdistselect.cpp" line="70"/>
        <location filename="customdistrolst.cpp" line="117"/>
        <source>Distro Name 8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="70"/>
        <source>default-distro8version-1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="71"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.distro8site.org&quot;&gt;http://www.distro8site.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; This distro 8 is unique and great in these ways.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; You will have to do this to install this distribution 8.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>distro8version-1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-1_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-1_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-1_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-2_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-2_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-2_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-3_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-3_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-3_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-4_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-4_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-4_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-5_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-5_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-5_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-6_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-6_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-6_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-7_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-7_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-7_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-8_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-8_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-8_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-9_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-9_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-9_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="76"/>
        <location filename="customdistselect.cpp" line="78"/>
        <location filename="customdistrolst.cpp" line="130"/>
        <source>Distro Name 9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="78"/>
        <source>default-distro9version-1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="79"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.distro9site.org&quot;&gt;http://www.distro9site.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; This distro 9 is unique and great in these ways.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; You will have to do this to install this distribution 9.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>distro9version-1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-1_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-1_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-1_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-2_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-2_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-2_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-3_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-3_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-3_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-4_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-4_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-4_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-5_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-5_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-5_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-6_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-6_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-6_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-7_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-7_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-7_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-8_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-8_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-8_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-9_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-9_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-9_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistrolst.cpp" line="14"/>
        <location filename="customdistrolst.cpp" line="15"/>
        <source>amd64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistrolst.cpp" line="21"/>
        <location filename="customdistrolst.cpp" line="22"/>
        <source>i386</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistrolst.cpp" line="28"/>
        <source>distro1format-iso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistrolst.cpp" line="31"/>
        <location filename="customdistrolst.cpp" line="35"/>
        <source>http://distro1site.org/distro-release-%1/distro-architecture%2.iso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistrolst.cpp" line="41"/>
        <source>distro2format-iso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistrolst.cpp" line="44"/>
        <location filename="customdistrolst.cpp" line="48"/>
        <source>http://distro2site.org/distro-release-%1/distro-architecture%2.iso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistrolst.cpp" line="54"/>
        <source>distro3format-iso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistrolst.cpp" line="57"/>
        <location filename="customdistrolst.cpp" line="61"/>
        <source>http://distro3site.org/distro-release-%1/distro-architecture%2.iso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistrolst.cpp" line="67"/>
        <source>distro4format-iso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistrolst.cpp" line="70"/>
        <location filename="customdistrolst.cpp" line="74"/>
        <source>http://distro4site.org/distro-release-%1/distro-architecture%2.iso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistrolst.cpp" line="80"/>
        <source>distro5format-iso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistrolst.cpp" line="83"/>
        <location filename="customdistrolst.cpp" line="87"/>
        <source>http://distro5site.org/distro-release-%1/distro-architecture%2.iso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistrolst.cpp" line="93"/>
        <source>distro6format-iso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistrolst.cpp" line="96"/>
        <location filename="customdistrolst.cpp" line="100"/>
        <source>http://distro6site.org/distro-release-%1/distro-architecture%2.iso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistrolst.cpp" line="106"/>
        <source>distro7format-iso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistrolst.cpp" line="109"/>
        <location filename="customdistrolst.cpp" line="113"/>
        <source>http://distro7site.org/distro-release-%1/distro-architecture%2.iso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistrolst.cpp" line="119"/>
        <source>distro8format-iso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistrolst.cpp" line="122"/>
        <location filename="customdistrolst.cpp" line="126"/>
        <source>http://distro8site.org/distro-release-%1/distro-architecture%2.iso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistrolst.cpp" line="132"/>
        <source>distro9format-iso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistrolst.cpp" line="135"/>
        <location filename="customdistrolst.cpp" line="139"/>
        <source>http://distro9site.org/distro-release-%1/distro-architecture%2.iso</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>unetbootin</name>
    <message>
        <location filename="unetbootin.cpp" line="218"/>
        <location filename="unetbootin.cpp" line="319"/>
        <location filename="unetbootin.cpp" line="320"/>
        <location filename="unetbootin.cpp" line="389"/>
        <location filename="unetbootin.cpp" line="563"/>
        <location filename="unetbootin.cpp" line="3497"/>
        <location filename="unetbootin.cpp" line="3510"/>
        <location filename="unetbootin.cpp" line="3692"/>
        <location filename="unetbootin.cpp" line="4354"/>
        <source>Hard Disk</source>
        <translation>Disc dur</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="220"/>
        <location filename="unetbootin.cpp" line="316"/>
        <location filename="unetbootin.cpp" line="317"/>
        <location filename="unetbootin.cpp" line="391"/>
        <location filename="unetbootin.cpp" line="567"/>
        <location filename="unetbootin.cpp" line="749"/>
        <location filename="unetbootin.cpp" line="769"/>
        <location filename="unetbootin.cpp" line="1018"/>
        <location filename="unetbootin.cpp" line="1593"/>
        <location filename="unetbootin.cpp" line="1680"/>
        <location filename="unetbootin.cpp" line="2665"/>
        <location filename="unetbootin.cpp" line="2688"/>
        <location filename="unetbootin.cpp" line="3501"/>
        <location filename="unetbootin.cpp" line="3527"/>
        <location filename="unetbootin.cpp" line="3696"/>
        <location filename="unetbootin.cpp" line="4020"/>
        <location filename="unetbootin.cpp" line="4358"/>
        <source>USB Drive</source>
        <translation>Disc USB</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="221"/>
        <location filename="unetbootin.cpp" line="238"/>
        <location filename="unetbootin.cpp" line="239"/>
        <location filename="unetbootin.cpp" line="355"/>
        <location filename="unetbootin.cpp" line="681"/>
        <location filename="unetbootin.cpp" line="688"/>
        <location filename="unetbootin.cpp" line="689"/>
        <location filename="unetbootin.cpp" line="3586"/>
        <source>ISO</source>
        <translation>ISO</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="222"/>
        <location filename="unetbootin.cpp" line="234"/>
        <location filename="unetbootin.cpp" line="235"/>
        <location filename="unetbootin.cpp" line="361"/>
        <location filename="unetbootin.cpp" line="681"/>
        <location filename="unetbootin.cpp" line="693"/>
        <location filename="unetbootin.cpp" line="694"/>
        <location filename="unetbootin.cpp" line="3578"/>
        <source>Floppy</source>
        <translation>Disquet</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="255"/>
        <location filename="unetbootin.cpp" line="261"/>
        <location filename="unetbootin.cpp" line="265"/>
        <location filename="unetbootin.cpp" line="269"/>
        <location filename="unetbootin.cpp" line="273"/>
        <location filename="unetbootin.cpp" line="279"/>
        <location filename="unetbootin.cpp" line="307"/>
        <source>either</source>
        <translation>o bé</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="286"/>
        <source>LiveUSB persistence</source>
        <translation>Persistència al dispositiu USB autònom</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="301"/>
        <source>FAT32-formatted USB drive</source>
        <translation>Dispositiu USB amb format FAT32</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="305"/>
        <source>EXT2-formatted USB drive</source>
        <translation>Dispositiu USB amb format EXT2</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="681"/>
        <source>Open Disk Image File</source>
        <translation>Obre un fitxer d&apos;imatge de disc</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="681"/>
        <source>All Files</source>
        <translation>Tots els fitxers</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="703"/>
        <location filename="unetbootin.cpp" line="715"/>
        <location filename="unetbootin.cpp" line="727"/>
        <source>All Files (*)</source>
        <translation>Tots els fitxers (*)</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="703"/>
        <source>Open Kernel File</source>
        <translation>Obre el fitxer de nucli</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="715"/>
        <source>Open Initrd File</source>
        <translation>Obre el fitxer initrd</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="727"/>
        <source>Open Bootloader Config File</source>
        <translation>Obre el fitxer de configuració del carregador de l&apos;arrencada</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="753"/>
        <source>Insert a USB flash drive</source>
        <translation>Inseriu una unitat flash USB</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="754"/>
        <source>No USB flash drives were found. If you have already inserted a USB drive, try reformatting it as FAT32.</source>
        <translation>No s&apos;ha trobat cap unitat flash USB . Si heu inserit un dispositiu USB, proveu de formatar-lo com a FAT32.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="773"/>
        <source>%1 not mounted</source>
        <translation>%1 sense muntar</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="774"/>
        <source>You must first mount the USB drive %1 to a mountpoint. Most distributions will do this automatically after you remove and reinsert the USB drive.</source>
        <translation>Primer munteu la unitat USB %1 a un punt de muntatge. La majoria de les distribucions ho fan automàticament si la retireu i la torneu a inserir.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="789"/>
        <source>Select a distro</source>
        <translation>Seleccioneu una distribució</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="790"/>
        <source>You must select a distribution to load.</source>
        <translation>Seleccioneu una distribució per carregar</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="804"/>
        <source>Select a disk image file</source>
        <translation>Seleccioneu un fitxer d&apos;imatge de disc</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="805"/>
        <source>You must select a disk image file to load.</source>
        <translation>Seleccioneu un fitxer d&apos;imatge de disc per carregar.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="819"/>
        <source>Select a kernel and/or initrd file</source>
        <translation>Seleccioneu un nucli i/o el fitxer initrd</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="820"/>
        <source>You must select a kernel and/or initrd file to load.</source>
        <translation>Seleccioneu un nucli i/o un fitxer initrd per carregar-los.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="834"/>
        <source>Diskimage file not found</source>
        <translation>No s&apos;ha trobat el fitxer d&apos;imatge de disc</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="835"/>
        <source>The specified diskimage file %1 does not exist.</source>
        <translation>El fitxer d&apos;imatge de disc %1 indicat no existeix.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="849"/>
        <source>Kernel file not found</source>
        <translation>No s&apos;ha trobat el fitxer del nucli</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="850"/>
        <source>The specified kernel file %1 does not exist.</source>
        <translation>El fitxer del nucli %1 indicat no existeix.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="864"/>
        <source>Initrd file not found</source>
        <translation>No s&apos;ha trobat el fitxer initrd</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="865"/>
        <source>The specified initrd file %1 does not exist.</source>
        <translation>El fitxer initrd %1 indicat no existeix.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="964"/>
        <source>%1 exists, overwrite?</source>
        <translation>%1 ja existeix. El voleu sobreescriure?</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="965"/>
        <source>The file %1 already exists. Press &apos;Yes to All&apos; to overwrite it and not be prompted again, &apos;Yes&apos; to overwrite files on an individual basis, and &apos;No&apos; to retain your existing version. If in doubt, press &apos;Yes to All&apos;.</source>
        <translation>El fitxer %1 ja existeix. Premeu «Sí a tot» per sobreescriure&apos;l i evitar la mateixa pregunta, «Sí» per sobreescriure els fitxers individualment, i «No» per conservar la versió existent. En cas de dubte premeu «Sí a tot».</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="991"/>
        <source>%1 is out of space, abort installation?</source>
        <translation>No hi ha prou espai a %1, voleu aturar la instal·lació?</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="992"/>
        <source>The directory %1 is out of space. Press &apos;Yes&apos; to abort installation, &apos;No&apos; to ignore this error and attempt to continue installation, and &apos;No to All&apos; to ignore all out-of-space errors.</source>
        <translation>No hi ha prou espai a la carpeta %1. Premeu «Sí» per aturar la instal·lació, «No» per ignorar aquest error i intentar continuar, i «No a tot» per ignorar tots els errors de manca d&apos;espai.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1086"/>
        <source>Locating kernel file in %1</source>
        <translation>S&apos;està localitzant el fitxer del nucli a %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1137"/>
        <source>Copying kernel file from %1</source>
        <translation>S&apos;està copiant el fitxer del nucli des de %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1143"/>
        <source>Locating initrd file in %1</source>
        <translation>S&apos;està localitzant el fitxer initrd a %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1184"/>
        <source>Copying initrd file from %1</source>
        <translation>S&apos;està copiant el fitxer initrd des de %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1190"/>
        <location filename="unetbootin.cpp" line="1270"/>
        <source>Extracting bootloader configuration</source>
        <translation>S&apos;està extraient la configuració del carregador de l&apos;arrencada</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1499"/>
        <location filename="unetbootin.cpp" line="1525"/>
        <source>&lt;b&gt;Extracting compressed iso:&lt;/b&gt; %1</source>
        <translation>&lt;b&gt;S&apos;està extraient el fitxer ISO comprimit:&lt;/b&gt; %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1779"/>
        <source>Copying file, please wait...</source>
        <translation>S&apos;està copiant els fitxers...</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1780"/>
        <location filename="unetbootin.cpp" line="2595"/>
        <source>&lt;b&gt;Source:&lt;/b&gt; &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</source>
        <translation>&lt;b&gt;Origen:&lt;/b&gt; &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1781"/>
        <location filename="unetbootin.cpp" line="2596"/>
        <source>&lt;b&gt;Destination:&lt;/b&gt; %1</source>
        <translation>&lt;b&gt;Destinació:&lt;/b&gt; %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1782"/>
        <source>&lt;b&gt;Copied:&lt;/b&gt; 0 bytes</source>
        <translation>&lt;b&gt;S&apos;ha copiat:&lt;/b&gt; 0 bytes</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1820"/>
        <source>Extracting files, please wait...</source>
        <translation>S&apos;està extraient els fitxers, espereu...</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1821"/>
        <source>&lt;b&gt;Archive:&lt;/b&gt; %1</source>
        <translation>&lt;b&gt;Arxiu:&lt;/b&gt; %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1822"/>
        <source>&lt;b&gt;Source:&lt;/b&gt;</source>
        <translation>&lt;b&gt;Origen:&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1823"/>
        <source>&lt;b&gt;Destination:&lt;/b&gt;</source>
        <translation>&lt;b&gt;Destinació:&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1824"/>
        <source>&lt;b&gt;Extracted:&lt;/b&gt; 0 of %1 files</source>
        <translation>&lt;b&gt;S&apos;ha extret:&lt;/b&gt; 0 of %1 files</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1827"/>
        <source>&lt;b&gt;Source:&lt;/b&gt; %1 (%2)</source>
        <translation>&lt;b&gt;Origen:&lt;/b&gt; %1 (%2)</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1828"/>
        <source>&lt;b&gt;Destination:&lt;/b&gt; %1%2</source>
        <translation>&lt;b&gt;Destinació:&lt;/b&gt; %1%2</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1829"/>
        <source>&lt;b&gt;Extracted:&lt;/b&gt; %1 of %2 files</source>
        <translation>&lt;b&gt;S&apos;ha extret:&lt;/b&gt; %1 of %2 files</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2594"/>
        <source>Downloading files, please wait...</source>
        <translation>S&apos;està descarregant els fitxers, espereu...</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2597"/>
        <source>&lt;b&gt;Downloaded:&lt;/b&gt; 0 bytes</source>
        <translation>&lt;b&gt;S&apos;ha descarregat:&lt;/b&gt; 0 bytes</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2757"/>
        <source>Download of %1 %2 from %3 failed. Please try downloading the ISO file from the website directly and supply it via the diskimage option.</source>
        <translation>Ha fallat la descàrrega de %1 %2 de %3. Intenteu baixar-vos el fitxer ISO directament des del lloc web i subministreu-lo mitjançant l&apos;opció «diskimage».</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2778"/>
        <location filename="unetbootin.cpp" line="2793"/>
        <source>&lt;b&gt;Downloaded:&lt;/b&gt; %1 of %2</source>
        <translation>&lt;b&gt;S&apos;ha descarregat:&lt;/b&gt; %1 de %2</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2808"/>
        <source>&lt;b&gt;Copied:&lt;/b&gt; %1 of %2</source>
        <translation>&lt;b&gt;S&apos;ha copiat:&lt;/b&gt; %1 de %2</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2912"/>
        <source>Searching in &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</source>
        <translation>S&apos;està cercant a &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2916"/>
        <source>%1/%2 matches in &lt;a href=&quot;%3&quot;&gt;%3&lt;/a&gt;</source>
        <translation>%1/%2 coincidències de &lt;a href=&quot;%3&quot;&gt;%3&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3170"/>
        <source>%1 not found</source>
        <translation>No s&apos;ha trobat %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3171"/>
        <source>%1 not found. This is required for %2 install mode.
Install the &quot;%3&quot; package or your distribution&apos;s equivalent.</source>
        <translation>No s&apos;ha trobat %1. És necessari per al mode d&apos;instal·lació %2.
Instal·leu el paquet «%3» o l&apos;equivalent de la vostra distribució.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3478"/>
        <source>(Current)</source>
        <translation>(Actual)</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3479"/>
        <source>(Done)</source>
        <translation>(Fet)</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3771"/>
        <source>Configuring grub2 on %1</source>
        <translation>S&apos;està configurant el grub2 a %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3783"/>
        <source>Configuring grldr on %1</source>
        <translation>S&apos;està configurant el grldr a %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3811"/>
        <source>Configuring grub on %1</source>
        <translation>S&apos;està configurant el grub a %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4109"/>
        <source>Installing syslinux to %1</source>
        <translation>S&apos;està instal·lant syslinux a %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4144"/>
        <source>Installing extlinux to %1</source>
        <translation>S&apos;està instal·lant extlinux a %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4306"/>
        <source>Syncing filesystems</source>
        <translation>S&apos;està sincronitzant els fitxers de sistema</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4311"/>
        <source>Setting up persistence</source>
        <translation>Configura la persistència</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4356"/>
        <source>After rebooting, select the </source>
        <translation>Després d&apos;arrencar de nou, seleccioneu la </translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4361"/>
        <source>After rebooting, select the USB boot option in the BIOS boot menu.%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>After rebooting, select the USB boot option in the BIOS boot menu.%1
Reboot now?</source>
        <translation type="vanished">Després d&apos;arrencar de nou, seleccioneu l&apos;opció d&apos;arrencada des de l&apos;USB al menú de la BIOS.%1
Voleu arrencar de nou ara?</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4364"/>
        <source>The created USB device will not boot off a Mac. Insert it into a PC, and select the USB boot option in the BIOS boot menu.%1</source>
        <translation>El dispositiu USB que s&apos;ha creat no arrencarà a un Mac. Inseriu-lo a un PC i seleccioneu l&apos;opció d&apos;engegada des del dispositiu USB a la BIOS.%1</translation>
    </message>
    <message>
        <location filename="distrolst.cpp" line="50"/>
        <source>
*IMPORTANT* Before rebooting, place an Ubuntu alternate (not desktop) install iso file on the root directory of your hard drive or USB drive. These can be obtained from cdimage.ubuntu.com</source>
        <translation>
*IMPORTANT* Abans d&apos;arrencar de nou, col·loqueu el fitxer iso d&apos;instal·lació de l&apos;Ubuntu Alternate (no la versió d&apos;escriptori) a la carpeta arrel del disc dur o de la unitat USB. Es pot aconseguir a cdimage.ubuntu.com</translation>
    </message>
    <message>
        <location filename="distrolst.cpp" line="264"/>
        <source>
*IMPORTANT* After rebooting, ignore any error messages and select back if prompted for a CD, then go to the main menu, select the &apos;Start Installation&apos; option, choose &apos;Network&apos; as the source, choose &apos;HTTP&apos; as the protocol, enter &apos;mirrors.kernel.org&apos; when prompted for a server, and enter &apos;/centos/%1/os/%2&apos; when asked for the folder.</source>
        <translation>
*IMPORTANT* Després d&apos;arrencar de nou, ignoreu qualsevol missatge d&apos;error i seleccioneu «Enrere» si es demana un CD. Tot seguit, des del menú principal, trieu l&apos;opció «Inicia la instal·lació», «Xarxa» com a origen, «HTTP» com a protocol, «mirrors.kernel.org» quan es demani el servidor i «/centos/%1/os/%2» quan es demani la carpeta.</translation>
    </message>
    <message>
        <location filename="distrolst.cpp" line="321"/>
        <source>
*IMPORTANT* Before rebooting, place a Debian install iso file on the root directory of your hard drive or USB drive. These can be obtained from cdimage.debian.org</source>
        <translation>
*IMPORTANT* Abans d&apos;arrencar de nou, situeu el fitxer iso Debian d&apos;instal·lació a la carpeta arrel del disc dur o unitat USB. Es pot aconseguir de cdimage.debian.com</translation>
    </message>
    <message>
        <location filename="distrolst.cpp" line="405"/>
        <source>
*IMPORTANT* After rebooting, ignore any error messages and select back if prompted for a CD, then go to the main menu, select the &apos;Start Installation&apos; option, choose &apos;Network&apos; as the source, choose &apos;HTTP&apos; as the protocol, enter &apos;download.fedora.redhat.com&apos; when prompted for a server, and enter &apos;/pub/fedora/linux/development/%1/os&apos; when asked for the folder.</source>
        <translation>
*IMPORTANT* Després d&apos;arrencar de nou, ignoreu qualsevol missatge d&apos;error i seleccioneu «Enrere» si es demana un CD. Tot seguit, des del menú principal, trieu l&apos;opció «Inicia la instal·lació», «Xarxa» com a origen, «HTTP» com a protocol, «download.fedora.redhat.com» quan es demani el servidor i «/pub/fedora/linux/development/%1/os» quan es demani la carpeta.</translation>
    </message>
    <message>
        <location filename="distrolst.cpp" line="411"/>
        <source>
*IMPORTANT* After rebooting, ignore any error messages and select back if prompted for a CD, then go to the main menu, select the &apos;Start Installation&apos; option, choose &apos;Network&apos; as the source, choose &apos;HTTP&apos; as the protocol, enter &apos;download.fedora.redhat.com&apos; when prompted for a server, and enter &apos;/pub/fedora/linux/releases/%1/Fedora/%2/os&apos; when asked for the folder.</source>
        <translation>
*IMPORTANT* Després d&apos;arrencar de nou, ignoreu qualsevol missatge d&apos;error i seleccioneu «Enrere» si es demana un CD. Tot seguit, des del menú principal, trieu l&apos;opció «Inicia la instal·lació», «Xarxa» com a origen, «HTTP» com a protocol, «download.fedora.redhat.com» quan es demani el servidor i «/pub/fedora/linux/releases/%1/Fedora/%2/os» quan es demani la carpeta.</translation>
    </message>
    <message>
        <location filename="distrolst.cpp" line="766"/>
        <source>
*IMPORTANT* After rebooting, ignore any error messages and select back if prompted for a CD, then go to the main menu, select the &apos;Start Installation&apos; option, choose &apos;Network&apos; as the source, choose &apos;HTTP&apos; as the protocol, enter &apos;download.opensuse.org&apos; when prompted for a server, and enter &apos;/factory/repo/oss&apos; when asked for the folder.</source>
        <translation>
*IMPORTANT* Després d&apos;arrencar de nou, ignoreu qualsevol missatge d&apos;error i seleccioneu «Enrere» si es demana un CD. Tot seguit, des del menú principal, trieu l&apos;opció «Inicia la instal·lació», «Xarxa» com a origen, «HTTP» com a protocol, «download.opensuse.org» quan es demani el servidor i «/factory/repo/oss» quan es demani la carpeta.</translation>
    </message>
    <message>
        <location filename="distrolst.cpp" line="772"/>
        <source>
*IMPORTANT* After rebooting, ignore any error messages and select back if prompted for a CD, then go to the main menu, select the &apos;Start Installation&apos; option, choose &apos;Network&apos; as the source, choose &apos;HTTP&apos; as the protocol, enter &apos;download.opensuse.org&apos; when prompted for a server, and enter &apos;/distribution/%1/repo/oss&apos; when asked for the folder.</source>
        <translation>
*IMPORTANT* Després d&apos;arrencar de nou, ignoreu qualsevol missatge d&apos;error i seleccioneu «Enrere» si es demana un CD. Tot seguit, des del menú principal, trieu l&apos;opció «Inicia la instal·lació», «Xarxa» com a origen, «HTTP» com a protocol, «download.opensuse.org» quan es demani el servidor i «/distribution/%1/repo/oss» quan es demani la carpeta.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="19"/>
        <location filename="unetbootin.cpp" line="785"/>
        <source>== Select Distribution ==</source>
        <translation>== Trieu la distribució ==</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="19"/>
        <location filename="distrover.cpp" line="23"/>
        <source>== Select Version ==</source>
        <translation>== Trieu la versió ==</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="20"/>
        <source>Welcome to &lt;a href=&quot;http://unetbootin.sourceforge.net/&quot;&gt;UNetbootin&lt;/a&gt;, the Universal Netboot Installer. Usage:&lt;ol&gt;&lt;li&gt;Select a distribution and version to download from the list above, or manually specify files to load below.&lt;/li&gt;&lt;li&gt;Select an installation type, and press OK to begin installing.&lt;/li&gt;&lt;/ol&gt;</source>
        <translation>Benvingut a &lt;a href=&quot;http://unetbootin.sourceforge.net/&quot;&gt;UNetbootin&lt;/a&gt;, l&apos;instal·lador universal Netboot. Sintaxi:&lt;ol&gt;&lt;li&gt;Seleccioneu una distribució i versió a descarregar de la llista anterior o indiqueu manualment els fitxers a carregar a baix.&lt;/li&gt;&lt;li&gt;Seleccioneu un tipus d&apos;instal·lació, i premeu «D&apos;acord» per començar la instal·lació.&lt;/li&gt;&lt;/ol&gt;</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="25"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.archlinux.org/&quot;&gt;http://www.archlinux.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Arch Linux is a lightweight distribution optimized for speed and flexibility.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The default version allows for installation over the internet (FTP).</source>
        <translation>&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://www.archlinux.org/&quot;&gt;http://www.archlinux.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt;Arch Linux és una distribució lleugera optimitzada en velocitat i flexibilitat.&lt;br/&gt;&lt;b&gt;Notes de la instal·lació:&lt;/b&gt; La versió predeterminada permet la instal·lació per internet (FTP).</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="30"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.backtrack-linux.org/&quot;&gt;http://www.backtrack-linux.org/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; BackTrack is a distribution focused on network analysis and penetration testing.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; BackTrack is booted and run in live mode; no installation is required to use it.</source>
        <translation>&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://www.backtrack-linux.org/&quot;&gt;http://www.backtrack-linux.org/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; BackTrack és una distribució enfocada a l&apos;anàlisi de la seguretat de la xarxa.&lt;br/&gt;&lt;b&gt;Notes de la instal·lació:&lt;/b&gt; La distribució BackTrack s&apos;executa en mode autònom i no cal instal·lació per utilitzar-la.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="35"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.centos.org/&quot;&gt;http://www.centos.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; CentOS is a free Red Hat Enterprise Linux clone.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The default version allows for both installation over the internet (FTP), or offline installation using pre-downloaded installation ISO files.</source>
        <translation>&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://www.centos.org/&quot;&gt;http://www.centos.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; CentOS és un clon lliure del Red Hat Enterprise Linux .&lt;br/&gt;&lt;b&gt;Notes de la instal·lació:&lt;/b&gt; La versió prdeterminada permet la instal·lació utilitzant internet (FTP) o fora de línia amb fitxers ISO descarregats prèviament.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="40"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://clonezilla.org/&quot;&gt;http://clonezilla.org/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; CloneZilla is a distribution used for disk backup and imaging.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; CloneZilla is booted and run in live mode; no installation is required to use it.</source>
        <translation>&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://clonezilla.org/&quot;&gt;http://clonezilla.org/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; CloneZilla és una distribució pensada per la creació i restauració de còpies de seguretat.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; La distribució CloneZilla s&apos;executa en mode autònom i no cal instal·lació per utilitzar-la.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="45"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://damnsmalllinux.org/&quot;&gt;http://damnsmalllinux.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Damn Small Linux is a minimalist distribution designed for older computers.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version loads the entire system into RAM and boots from memory, so installation is not required but optional.</source>
        <translation>&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://damnsmalllinux.org/&quot;&gt;http://damnsmalllinux.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; Damn Small Linux és una distribució minimalista dissenyada per a ordinadors antics.&lt;br/&gt;&lt;b&gt;Notes de la instal·lació:&lt;/b&gt; La versió autònoma carrega tot el sistema a la RAM i arrenca des de la memòria, per tant la instal·lació no és necessària sinó opcional.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="50"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.debian.org/&quot;&gt;http://www.debian.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Debian is a community-developed Linux distribution that supports a wide variety of architectures and offers a large repository of packages.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The NetInstall version allows for installation over FTP. If you would like to use a pre-downloaded install iso, use the HdMedia option, and then place the install iso file on the root directory of your hard drive or USB drive</source>
        <translation>&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://www.debian.org/&quot;&gt;http://www.debian.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; Debian és una distribució creada per la comunitat que suporta un ventall d&apos;arquitectures i ofereix una gran quantitat de programari.&lt;br/&gt;&lt;b&gt;Notes de la instal·lació:&lt;/b&gt; La versió «NetInstall» permet ser instal·lada mitjançant FTP. Si desitgeu instal·lar-la des d&apos;una imatge iso prèviament descarregada, seleccioneu l&apos;opció «HdMedia» i copieu el fitxer iso a l&apos;arrel de la unitat USB o disc dur que emprareu per a la instal·lació.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="56"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.dreamlinux.com.br/&quot;&gt;http://www.dreamlinux.com.br&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Dreamlinux is a user-friendly Debian-based distribution.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched.</source>
        <translation>&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://www.dreamlinux.com.br/&quot;&gt;http://www.dreamlinux.com.br&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; Dreamlinux és una distribució amigable basada en Debian.&lt;br/&gt;&lt;b&gt;Notes d&apos;instal·lació:&lt;/b&gt; La versió autònoma permet arrencar en mode autònom, des d&apos;on hi ha l&apos;opció d&apos;executar l&apos;instal·lador.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="61"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.freedrweb.com/livecd&quot;&gt;http://www.freedrweb.com/livecd&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Dr.Web AntiVirus is an anti-virus emergency kit to restore a system that broke due to malware.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which malware scans can be launched.</source>
        <translation>&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://www.freedrweb.com/livecd&quot;&gt;http://www.freedrweb.com/livecd&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; Dr.Web AntiVirus és una utilitat d&apos;emergència en cas d&apos;infecció vírica al vostre sistema.&lt;br/&gt;&lt;b&gt;Notes de la instal·lació:&lt;/b&gt; La versió en mode autònom us permet mitjançant aquest estat analitzar el vostre sistema sense necessitat d&apos;instal·lar-hi res al disc dur.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="66"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.elivecd.org/&quot;&gt;http://www.elivecd.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Elive is a Debian-based distribution featuring the Enlightenment window manager.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched.</source>
        <translation>&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://www.elivecd.org/&quot;&gt;http://www.elivecd.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; Elive és una distribució oficial basada en Debian que incorpora el gestor d&apos;escriptori Enlightenment.&lt;br/&gt;&lt;b&gt;Notes de la Instal·lació:&lt;/b&gt; El mode autònom permet provar el sistema sense instal·lar res al vostre ordinador, amb opció a ser també instal·lada.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="71"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://fedoraproject.org/&quot;&gt;http://fedoraproject.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Fedora is a Red Hat sponsored community distribution which showcases the latest cutting-edge free/open-source software.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched. The NetInstall version allows for both installation over the internet (FTP), or offline installation using pre-downloaded installation ISO files.</source>
        <translation>&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://fedoraproject.org/&quot;&gt;http://fedoraproject.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; Fedora és una distribució d&apos;una comunitat patrocinada per Red Hat que mostra l&apos;avantguarda més recent en programari de codi obert.&lt;br/&gt;&lt;b&gt;Notes de la instal·lació:&lt;/b&gt; La versió autònoma permet arrencar en mode autònom, des d&apos;on hi ha l&apos;opció d&apos;executar l&apos;instal·lador. La versió NetInstall permet la instal·lació utilitzant internet (FTP) o fora de línia amb fitxers ISO descarregats prèviament.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="76"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.freebsd.org/&quot;&gt;http://www.freebsd.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; FreeBSD is a general-purpose Unix-like operating system designed for scalability and performance.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The default version allows for both installation over the internet (FTP), or offline installation using pre-downloaded installation ISO files.</source>
        <translation>&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://www.freebsd.org/&quot;&gt;http://www.freebsd.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; FreeBSD és un sistema operatiu d&apos;ús general semblant a l&apos;Unix dissenyat per ser escalable i ràpid.&lt;br/&gt;&lt;b&gt;Notes de la instal·lació:&lt;/b&gt; La versió predeterminada permet la instal·lació utilitzant internet (FTP) o fora de línia amb fitxers ISO descarregats prèviament.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="81"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.freedos.org/&quot;&gt;http://www.freedos.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; FreeDOS is a free MS-DOS compatible operating system.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; See the &lt;a href=&quot;http://fd-doc.sourceforge.net/wiki/index.php?n=FdDocEn.FdInstall&quot;&gt;manual&lt;/a&gt; for installation details.</source>
        <translation>&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://www.freedos.org/&quot;&gt;http://www.freedos.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; FreeDOS és un sistema operatiu lliure compatible amb MS-DOS.&lt;br/&gt;&lt;b&gt;Notes de la instal·lació:&lt;/b&gt; Vegeu el &lt;a href=&quot;http://fd-doc.sourceforge.net/wiki/index.php?n=FdDocEn.FdInstall&quot;&gt;manual&lt;/a&gt; per als detalls de la instal·lació.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="86"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://freenas.org/&quot;&gt;http://www.freenas.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; FreeNAS is an embedded open source NAS (Network-Attached Storage) distribution based on FreeBSD.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The LiveCD version creates a RAM drive for FreeNAS, and uses a FAT formatted floppy disk or USB key for saving the configuration file. The embedded version allows installation to hard disk.</source>
        <translation>&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://freenas.org/&quot;&gt;http://www.freenas.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; FreeNAS és una distribució NAS (Network-Attached Storage) de codi obert basada en FreeBSD.&lt;br/&gt;&lt;b&gt;Notes d&apos;instal·lació:&lt;/b&gt; La versió de disc autònom crea una unitat RAM per al FreeNAS i utilitza un disquet o un disc USB en format FAT per desar el fitxer de configuració. La versió incorporada permet la instal·lació al disc dur.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="91"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://frugalware.org/&quot;&gt;http://frugalware.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Frugalware is a general-purpose Slackware-based distro for advanced users.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The default option allows for both installation over the internet (FTP), or offline installation using pre-downloaded installation ISO files.</source>
        <translation>&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://frugalware.org/&quot;&gt;http://frugalware.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; Frugalware és una distribució d&apos;ús general basada en Slackware per a usuaris avançats.&lt;br/&gt;&lt;b&gt;Notes de la instal·lació:&lt;/b&gt; L&apos;opció predeterminada permet la instal·lació utilitzant internet (FTP) o fora de línia amb fitxers ISO descarregats prèviament.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="101"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.geexbox.org/&quot;&gt;http://www.geexbox.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; GeeXboX is an Embedded Linux Media Center Distribution.&lt;br/&gt;</source>
        <translation>&lt;b&gt;Pàgina inicial:&lt;/b&gt; &lt;a href=&quot;http://www.geexbox.org/&quot;&gt;http://www.geexbox.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; GeeXboX és una distribució amb el Linux Media Center incrustat.&lt;br/&gt;</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="114"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.gnewsense.org/&quot;&gt;http://www.gnewsense.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; gNewSense is an FSF-endorsed distribution based on Ubuntu with all non-free components removed.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched.</source>
        <translation>&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://www.gnewsense.org/&quot;&gt;http://www.gnewsense.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; gNewSense és una distribució aprovada per la FSF basada en Ubuntu amb tots els components que no són lliures suprimits.&lt;br/&gt;&lt;b&gt;Notes de la instal·lació:&lt;/b&gt; La versió autònoma permet arrencar en mode autònom, des d&apos;on hi ha l&apos;opció d&apos;executar l&apos;instal·lador.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="119"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://gujin.sourceforge.net/&quot;&gt;http://gujin.sourceforge.net&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Gujin is a graphical boot manager which can bootstrap various volumes and files.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; Gujin simply boots and runs; no installation is required to use it.</source>
        <translation>&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://gujin.sourceforge.net/&quot;&gt;http://gujin.sourceforge.net&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; Gujin és un gestor d&apos;arrencada gràfic que pot arrencar diversos volums i fitxers.&lt;br/&gt;&lt;b&gt;Notes de la instal·lació:&lt;/b&gt; Gujin només arrenca i executa, no cal cap instal·lació per utilitzar-lo.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="124"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://ftp.kaspersky.com/devbuilds/RescueDisk/&quot;&gt;http://ftp.kaspersky.com/devbuilds/RescueDisk/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Kaspersky Rescue Disk detects and removes malware from your Windows installation.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which malware scans can be launched.</source>
        <translation>&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://ftp.kaspersky.com/devbuilds/RescueDisk/&quot;&gt;http://ftp.kaspersky.com/devbuilds/RescueDisk/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; Kaspersky Rescue Disk detecta i suprimeix programari maliciós de la vostra instal·lació del Windows.&lt;br/&gt;&lt;b&gt;Notes d&apos;instal·lació:&lt;b/&gt; La versió autònoma permet arrencar en mode autònom i executar els escanejos de programari maliciós.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="129"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.kubuntu.org/&quot;&gt;http://www.kubuntu.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Kubuntu is an official Ubuntu derivative featuring the KDE desktop.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched. The NetInstall version allows for installation over FTP, and can install Kubuntu and other official Ubuntu derivatives. If you would like to use a pre-downloaded alternate (not desktop) install iso, use the HdMedia option, and then place the alternate install iso file on the root directory of your hard drive or USB drive</source>
        <translation>&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://www.kubuntu.org/&quot;&gt;http://www.kubuntu.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; Kubuntu és una distribució oficial derivada d&apos;Ubuntu que incorpora el gestor d&apos;escriptori KDE.&lt;br/&gt;&lt;b&gt;Notes de la instal·lació:&lt;/b&gt; El mode autònom permet provar Kubuntu sense instal·lar res al vostre ordinador, amb opció a ser també instal·lada. La versió «NetInstall» permet instal·lar Kubuntu i altres derivats oficials d&apos;Ubuntu mitjançant FTP. Si desitgeu instal·lar-la des d&apos;una imatge iso prèviament descarregada, seleccioneu l&apos;opció «HdMedia» i copieu el fitxer iso a l&apos;arrel de la unitat USB o disc dur que emprareu per a la instal·lació.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="134"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://linuxconsole.org/&quot;&gt;http://linuxconsole.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; LinuxConsole is a desktop distro to play games, easy to install, easy to use and fast to boot .&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The 1.0.2010 is latest 1.0, now available on rolling release (run liveuptate to update modules and kernel).</source>
        <translation>&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://linuxconsole.org/&quot;&gt;http://linuxconsole.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; LinuxConsole és una distribució d&apos;escriptori per jugar, fàcil d&apos;instal·lar i d&apos;utilitzar, i que s&apos;inicia ràpidament.&lt;br/&gt;&lt;b&gt;Notes de la instal·ació:&lt;/b&gt; La versió 1.0.2010 es la darrera 1.0, ara està disponible com a distribució en evolució constant (executeu el Liveupdate per actualitzar els mòduls i el nucli).</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="139"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://linuxmint.com/&quot;&gt;http://linuxmint.com&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Linux Mint is a user-friendly Ubuntu-based distribution which includes additional proprietary codecs and other software by default.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched.</source>
        <translation>&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://linuxmint.com/&quot;&gt;http://linuxmint.com&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció: Linux Mint és una distribució amigable basada en Ubuntu que inclou per defecte còdecs no lliures i d&apos;altre programari addicional.&lt;/b&gt; &lt;br/&gt;&lt;b&gt;Notes de la instal·lació:&lt;/b&gt; La versió autònoma permet arrencar en mode autònom, des d&apos;on hi ha l&apos;opció d&apos;executar l&apos;instal·lador.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="144"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.lubuntu.net/&quot;&gt;http://www.lubuntu.net&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Lubuntu is an official Ubuntu derivative featuring the LXDE desktop.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched. The NetInstall version allows for installation over FTP, and can install Kubuntu and other official Ubuntu derivatives. If you would like to use a pre-downloaded alternate (not desktop) install iso, use the HdMedia option, and then place the alternate install iso file on the root directory of your hard drive or USB drive</source>
        <translation>&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://www.lubuntu.net/&quot;&gt;http://www.lubuntu.net&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; Lubuntu és una derivació oficial d&apos;Ubuntu que utilitza LXDE com a escriptori.&lt;br/&gt;&lt;b&gt;Notes d&apos;instal·lació:&lt;/b&gt; La versió autònoma permet arrencar em mode autònom, des d&apos;on hi ha l&apos;opció d&apos;executar l&apos;instal·lador. La versió NetInstall permet la instal·lació utilitzant internet (FTP), i pot instal·lar Lubuntu i d&apos;altres derivats oficials d&apos;Ubuntu. Si voleu utilitzar una imatge iso baixada prèviament (sense escriptori), seleccioneu l&apos;opció «HdMedia» i copieu el fitxer iso a l&apos;arrel de la unitat USB o disc dur que emprareu per a la instal·lació.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="149"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.mandriva.com/&quot;&gt;http://www.mandriva.com/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Mandriva is a user-friendly distro formerly known as Mandrake Linux.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched. The NetInstall version allows for installation over the internet (FTP) or via pre-downloaded &lt;a href=&quot;http://www.mandriva.com/en/download&quot;&gt;&quot;Free&quot; iso image files&lt;/a&gt;.</source>
        <translation>&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://www.mandriva.com/&quot;&gt;http://www.mandriva.com/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; Mandriva és una distribució amigable coneguda prèviament com a Mandrake Linux.&lt;br/&gt;&lt;b&gt;Notes de la instal·lació:&lt;/b&gt; La versió autònoma permet arrencar en mode autònom, des d&apos;on hi ha l&apos;opció d&apos;executar l&apos;instal·lador. La versió NetInstall permet la instal·lació utilitzant internet (FTP) o amb fitxers ISO «gratuïts» descarregats prèviament &lt;a href=&quot;http://www.mandriva.com/en/download&quot;&gt;.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="154"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.mepis.org/&quot;&gt;http://www.mepis.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; MEPIS is a Debian-based distribution. SimplyMEPIS is a user-friendly version based on KDE, while AntiX is a lightweight version for older computers.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; MEPIS supports booting in Live mode, from which the installer can optionally be launched.</source>
        <translation>&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://www.mepis.org/&quot;&gt;http://www.mepis.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; MEPIS és una distribució basada en Debian. SimplyMEPIS és una versió amigable basada en KDE i AntiX és una versió lleugera per a ordinadors antics.&lt;br/&gt;&lt;b&gt;Notes de la instal·lació:&lt;/b&gt; MEPIS permet arrencar en mode autònom, des d&apos;on hi ha l&apos;opció d&apos;executar l&apos;instal·lador.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="159"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://netbootcd.tuxfamily.org/&quot;&gt;http://netbootcd.tuxfamily.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; NetbootCD is a small boot CD that downloads and boots network-based installers for other distributions.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; NetbootCD boots and runs in live mode.</source>
        <translation>&lt;b&gt;Pàgina inicial:&lt;/b&gt; &lt;a href=&quot;http://netbootcd.tuxfamily.org/&quot;&gt;http://netbootcd.tuxfamily.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; NetbootCD és un CD d&apos;arrencada petit que descarrega i arrenca instal·ladors basats en la xarxa per a altres distribucions.&lt;br/&gt;&lt;b&gt;Notes d&apos;instal·lació:&lt;/b&gt; NetbootCD arrenca i s&apos;executa en mode autònom.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="169"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.nimblex.net/&quot;&gt;http://www.nimblex.net&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; NimbleX is a small, versatile Slackware-based distribution. It is built using the linux-live scripts, and features the KDE desktop. It can be booted from CD or flash memory (USB pens or MP3 players), and can easily be customized and extended.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; NimbleX boots in Live mode.</source>
        <translation>&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://www.nimblex.net/&quot;&gt;http://www.nimblex.net&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; NimbleX és una distribució petita i versàtil basada en Slackware. Està contruïda utilitzant els sripts linux-live i té un escriptori KDE. Es pot arrencar des d&apos;un CD o d&apos;una memòria flash (dispositius USB o reproductors MP3) i es pot personalitzar i ampliar fàcilment. &lt;br/&gt;&lt;b&gt;Notes de la instal·lació:&lt;/b&gt; NimbleX arrenca en mode autònom.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="174"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://pogostick.net/~pnh/ntpasswd/&quot;&gt;http://pogostick.net/~pnh/ntpasswd/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; The Offline NT Password and Registry Editor can reset Windows passwords and edit the registry on Windows 2000-Vista.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; NTPasswd is booted and run in live mode; no installation is required to use it.</source>
        <translation>&lt;b&gt;Pàgina inicial:&lt;/b&gt; &lt;a href=&quot;http://pogostick.net/~pnh/ntpasswd/&quot;&gt;http://pogostick.net/~pnh/ntpasswd/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; Offline NT Password and Registry Editor pot reiniciar els passaports del Windows i editar el registre de Windows 2000-Vista.&lt;br/&gt;&lt;b&gt;Notes d&apos;instal·lació:&lt;/b&gt; NTPasswd arrenca i s&apos;executa en mode autònom; no cal cap instal·lació per utilitzar-lo.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="179"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.opensuse.org/&quot;&gt;http://www.opensuse.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; openSUSE is a user-friendly Novell sponsored distribution.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The default version allows for both installation over the internet (FTP), or offline installation using pre-downloaded installation ISO files.</source>
        <translation>&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://www.opensuse.org/&quot;&gt;http://www.opensuse.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; openSUSE és una distribució amigable patrocinada per Novell.&lt;br/&gt;&lt;b&gt;Notes de la instal·lació:&lt;/b&gt; La versió predeterminada permet la instal·lació utilitzant internet (FTP) o fora de línia amb fitxers ISO descarregats prèviament.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="184"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://ophcrack.sourceforge.net/&quot;&gt;http://ophcrack.sourceforge.net&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Ophcrack can crack Windows passwords.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; Ophcrack is booted and run in live mode; no installation is required to use it.</source>
        <translation>&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://ophcrack.sourceforge.net/&quot;&gt;http://ophcrack.sourceforge.net&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; Ophcrack pot desxifrar contrasenyes del Windows.&lt;br/&gt;&lt;b&gt;Notes de la instal·lació:&lt;/b&gt; Ophcrack arrenca i s&apos;executa, no cal cap instal·lació per utilitzar-lo.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="189"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://partedmagic.com/&quot;&gt;http://partedmagic.com&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Parted Magic includes the GParted partition manager and other system utilities which can resize, copy, backup, and manipulate disk partitions.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; Parted Magic is booted and run in live mode; no installation is required to use it.</source>
        <translation>&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://partedmagic.com/&quot;&gt;http://partedmagic.com&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; Parted Magic inclou el gestor de particions GParted i d&apos;altres eines del sistema que poden redimensionar, copiar, fe còpies de seguretat i manipular particions de disc.&lt;br/&gt;&lt;b&gt;Notes de la instal·lació:&lt;/b&gt; Parted Magic arrenca i s&apos;executa en mode autònom, no cal instal·lació per utilitzar-la.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="199"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.puppylinux.com/&quot;&gt;http://www.puppylinux.com&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Puppy Linux is a lightweight distribution designed for older computers.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version loads the entire system into RAM and boots from memory, so installation is not required but optional.</source>
        <translation>&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://www.puppylinux.com/&quot;&gt;http://www.puppylinux.com&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; Puppy Linux és una distribució lleugera dissenyada per a ordinadors antics.&lt;br/&gt;&lt;b&gt;Notes de la instal·lació:&lt;/b&gt; La versió autònoma carrega tot el sistema a la RAM i arrenca des de la memòria, per tant la instal·lació no és necessària sinó opcional.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="204"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.sabayonlinux.org/&quot;&gt;http://www.sabayonlinux.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Sabayon Linux is a Gentoo-based Live DVD distribution which features the Entropy binary package manager in addition to the source-based Portage.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched. The LiteMCE edition is 2 GB, while the full edition will need an 8 GB USB drive</source>
        <translation>&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://www.sabayonlinux.org/&quot;&gt;http://www.sabayonlinux.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; Sabayon Linux és una distribució basada en Gentoo que implementa el gestor de paquets binaris Entropy, a més del gestor basat en codi font Portage.&lt;br/&gt;&lt;b&gt;Notes de la instal·lació:&lt;/b&gt; La versió autònoma permet arrencar en mode autònom, des d&apos;on hi ha l&apos;opció d&apos;executar l&apos;instal·lador. L&apos;edició LiteMCE ocupa 2 GB, per a la versió completa cal un dispositiu USB de 8GB</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="209"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://salixos.org&quot;&gt;http://salixos.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Salix is a GNU/Linux distribution based on Slackware (fully compatible) that is simple, fast and easy to use.&lt;br/&gt;Like a bonsai, Salix is small, light &amp; the product of infinite care.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched.&lt;br/&gt;Default root password is &lt;b&gt;live&lt;/b&gt;.</source>
        <translation>&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://salixos.org&quot;&gt;http://salixos.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; Salix és una distribució GNU/Linux basada en Slackware (totalment compatible) que és senzilla, ràpida i fàcil d&apos;utilitzar.&lt;br/&gt;Com un bonsai, Salix és petita, lleugera i resultat d&apos;una cura infinita.&lt;br/&gt;&lt;b&gt;Notes d&apos;instal·lació:&lt;/b&gt; La versió autònoma permet arrencar en mode autònom, des d&apos;on hi ha l&apos;opció d&apos;executar l&apos;instal·lador.&lt;br/&gt;La contrasenya d&apos;usuari primari per defecte és &lt;b&gt;live&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="221"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.slax.org/&quot;&gt;http://www.slax.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Slax is a Slackware-based distribution featuring the KDE desktop.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched.</source>
        <translation>&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://www.slax.org/&quot;&gt;http://www.slax.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; Slax és una distribució basada en Slackware-based amb l&apos;escriptori KDE.&lt;br/&gt;&lt;b&gt;Notes de la instal·lació:&lt;/b&gt; La versió autònoma permet arrencar en mode autònom, des d&apos;on hi ha l&apos;opció d&apos;executar l&apos;instal·lador.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="226"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.slitaz.org/en/&quot;&gt;http://www.slitaz.org/en&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; SliTaz is a lightweight, desktop-oriented micro distribution.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version loads the entire system into RAM and boots from memory, so installation is not required but optional.</source>
        <translation>&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://www.slitaz.org/en/&quot;&gt;http://www.slitaz.org/en&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; SliTaz és una distribució lleugera pensada per a l&apos;ordinador de taula.&lt;br/&gt;&lt;b&gt;Notes de la instal·lació:&lt;/b&gt; La versió autònoma carrega tot el sistema a la RAM i arrenca des de la memòria, per tant la instal·lació no és necessària sinó opcional.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="231"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://btmgr.sourceforge.net/about.html&quot;&gt;http://btmgr.sourceforge.net/about.html&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Smart Boot Manager is a bootloader which can overcome some boot-related BIOS limitations and bugs.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; SBM simply boots and runs; no installation is required to use it.</source>
        <translation>&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://btmgr.sourceforge.net/about.html&quot;&gt;http://btmgr.sourceforge.net/about.html&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; Smart Boot Manager és un carregador de l&apos;arrencada que pot evitar algunes limitacions i errors de la BIOS.&lt;br/&gt;&lt;b&gt;Notes de la instal·lació:&lt;/b&gt; SBM només arrenca i executa, no cal cap instal·lació per utilitzar-lo.</translation>
    </message>
    <message>
        <location filename="distrovercust.cpp" line="12"/>
        <location filename="distrover.cpp" line="236"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.supergrubdisk.org&quot;&gt;http://www.supergrubdisk.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Super Grub Disk is a bootloader which can perform a variety of MBR and bootloader recovery tasks.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; SGD simply boots and runs; no installation is required to use it.</source>
        <translation>&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://www.supergrubdisk.org&quot;&gt;http://www.supergrubdisk.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; Super Grub Disk és un carregador de l&apos;arrencada que pot fer diverses tasques de recuperació del MBR i del carregador d&apos;arrencada.&lt;br/&gt;&lt;b&gt;Notes de la instal·lació:&lt;/b&gt; SGD només arrenca i executa, no cal cap instal·lació per utilitzar-lo.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="241"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://hacktolive.org/wiki/Super_OS&quot;&gt;http://hacktolive.org/wiki/Super_OS&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Super OS is an unofficial derivative of Ubuntu which includes additional software by default. Requires a 2GB USB drive to install.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched.</source>
        <translation>&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://hacktolive.org/wiki/Super_OS&quot;&gt;http://hacktolive.org/wiki/Super_OS&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; Super OS és una distribució no oficial derivada d&apos;Ubuntu que incorpora aplicacions adicionals. És necessari una unitat USB amb 2GB d&apos;espai per poder ser instal·lada.&lt;br/&gt;&lt;b&gt;Notes de la Instal·lació:&lt;/b&gt; La versió autònoma permet arrencar en mode autònom, des d&apos;on hi ha l&apos;opció d&apos;executar l&apos;instal·lador.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="251"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.ubuntu.com/&quot;&gt;http://www.ubuntu.com&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Ubuntu is a user-friendly Debian-based distribution. It is currently the most popular Linux desktop distribution.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched. The NetInstall version allows for installation over FTP, and can install Kubuntu and other official Ubuntu derivatives. If you would like to use a pre-downloaded alternate (not desktop) install iso, use the HdMedia option, and then place the alternate install iso file on the root directory of your hard drive or USB drive</source>
        <translation>&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://www.ubuntu.com/&quot;&gt;http://www.ubuntu.com&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; Ubuntu és una distribució amigable basada en Debian. Actualment és una de les distribucions més populars de Linux.&lt;br/&gt;&lt;b&gt;Notes de la instal·lació:&lt;/b&gt; La versió autònoma permet arrencar en mode autònom, des d&apos;on hi ha l&apos;opció d&apos;executar l&apos;instal·lador. La versió «NetInstall» permet instal·lar Ubuntu i altres derivats com ara Kubuntu mitjançant FTP. Si desitgeu instal·lar-la des d&apos;una imatge iso prèviament descarregada, seleccioneu l&apos;opció «HdMedia» i copieu el fitxer iso a l&apos;arrel de la unitat USB o disc dur que emprareu per a la instal·lació.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="256"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.xpud.org/&quot;&gt;http://www.xpud.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; xPUD is a lightweight distribution featuring a simple kiosk-like interface with a web browser and media player.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version loads the entire system into RAM and boots from memory.</source>
        <translation>&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://www.xpud.org/&quot;&gt;http://www.xpud.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; xPUD és una distribució lleugera amb una interfície senzilla tipus quiosc, amb un navegador i un reproductor multimèdia.&lt;br/&gt;&lt;b&gt;Notes de la instal·lació:&lt;/b&gt; La versió autònoma carrega tot el sistema a la RAM i arrenca des de la memòria.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="261"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.xubuntu.org/&quot;&gt;http://www.xubuntu.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Xubuntu is an official Ubuntu derivative featuring the XFCE desktop.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched. The NetInstall version allows for installation over FTP, and can install Kubuntu and other official Ubuntu derivatives. If you would like to use a pre-downloaded alternate (not desktop) install iso, use the HdMedia option, and then place the alternate install iso file on the root directory of your hard drive or USB drive</source>
        <translation>&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://www.xubuntu.org/&quot;&gt;http://www.xubuntu.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; Xubuntu és una distribució oficial derivada d&apos;Ubuntu que incorpora el gestor d&apos;escriptori XFCE.&lt;br/&gt;&lt;b&gt;Notes de la Instal·lació:&lt;/b&gt; La versió autònoma permet arrencar en mode autònom, des d&apos;on hi ha l&apos;opció d&apos;executar l&apos;instal·lador. La versió «NetInstall» permet instal·lar Xubuntu i altres derivats oficials d&apos;Ubuntu mitjançant FTP. Si desitgeu instal·lar-la des d&apos;una imatge iso prèviament descarregada, seleccioneu l&apos;opció «HdMedia» i copieu el fitxer iso a l&apos;arrel del dispositiu USB o disc dur que utilizeu per a la instal·lació.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="266"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.zenwalk.org/&quot;&gt;http://www.zenwalk.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Zenwalk is a Slackware-based distribution featuring the XFCE desktop.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched.</source>
        <translation>&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://www.zenwalk.org/&quot;&gt;http://www.zenwalk.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; Zenwalk és una distribució basada en Slackware que utilitza l&apos;escriptori XFCE.&lt;br/&gt;&lt;b&gt;Notes de la instal·lació:&lt;/b&gt; La versió autònoma permet arrencar en mode autònom, des d&apos;on hi ha l&apos;opció d&apos;executar l&apos;instal·lador.</translation>
    </message>
    <message>
        <location filename="distrovercust.cpp" line="33"/>
        <source>&lt;img src=&quot;:/eeepclos.png&quot; /&gt;&lt;br/&gt;&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.eeepclinuxos.com/&quot;&gt;http://www.eeepclinuxos.com&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; EeePCLinuxOS is a user-friendly PCLinuxOS based distribution for the EeePC.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; Make sure install media is empty and formatted before proceeding with install.</source>
        <translation>&lt;img src=&quot;:/eeepclos.png&quot; /&gt;&lt;br/&gt;&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://www.eeepclinuxos.com/&quot;&gt;http://www.eeepclinuxos.com&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; EeePCLinuxOS és una distribució amigable basada en PCLinuxOS per a l&apos;EeePC.&lt;br/&gt;&lt;b&gt;Notes de la instal·lació:&lt;/b&gt; El suport d&apos;instal·lació ha d&apos;estar buit i tenir format abans d&apos;instal·lar-lo.</translation>
    </message>
    <message>
        <location filename="distrovercust.cpp" line="41"/>
        <source>&lt;img src=&quot;:/eeeubuntu.png&quot; style=&quot;float:left;&quot; /&gt;&lt;br/&gt;&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.ubuntu-eee.com/&quot;&gt;http://www.ubuntu-eee.com&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Ubuntu Eee is not only Ubuntu optimized for the Asus Eee PC. It&apos;s an operating system, using the Netbook Remix interface, which favors the best software available instead of open source alternatives (ie. Skype instead of Ekiga).&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; Make sure install media is empty and formatted before proceeding with install.</source>
        <translation>&lt;img src=&quot;:/eeeubuntu.png&quot; style=&quot;float:left;&quot; /&gt;&lt;br/&gt;&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://www.ubuntu-eee.com/&quot;&gt;http://www.ubuntu-eee.com&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; Ubuntu Eee no és només l&apos;Ubuntu optimitzat per a l&apos;Asus Eee PC. És un sistema operatiu que fa servir la interfície del Netbook Remix, que prefereix el millor programari disponible en lloc de les alternatives en programari lliure (Skype en lloc d&apos;Ekiga, per exemple).&lt;br/&gt;&lt;b&gt;Notes de la instal·lació:&lt;/b&gt; El suport d&apos;instal·lació ha d&apos;estar buit i tenir format abans d&apos;instal·lar-lo.</translation>
    </message>
    <message>
        <location filename="distrovercust.cpp" line="53"/>
        <source>&lt;img src=&quot;:/elive.png&quot; /&gt;&lt;br/&gt;&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.elivecd.org/&quot;&gt;http://www.elivecd.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Elive is a Debian-based distribution featuring the Enlightenment window manager.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched.</source>
        <translation>&lt;img src=&quot;:/elive.png&quot; /&gt;&lt;br/&gt;&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://www.elivecd.org/&quot;&gt;http://www.elivecd.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; Elive és una distribució basada en Debian que incorpora el gestor de finestres Enlightenment.&lt;br/&gt;&lt;b&gt;Notes de la instal·lació:&lt;/b&gt; La versió autònoma permet arrencar en mode autònom, des d&apos;on hi ha l&apos;opció d&apos;executar l&apos;instal·lador.</translation>
    </message>
    <message>
        <location filename="distrovercust.cpp" line="61"/>
        <source>&lt;img src=&quot;:/kiwi_logo_ro.png&quot; /&gt;&lt;br/&gt;&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.kiwilinux.org/&quot;&gt;http://www.kiwilinux.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Kiwi Linux is an Ubuntu derivative primarily made for Romanian, Hungarian and English speaking users.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; Make sure install media is empty and formatted before proceeding with install.</source>
        <translation>&lt;img src=&quot;:/kiwi_logo_ro.png&quot; /&gt;&lt;br/&gt;&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://www.kiwilinux.org/&quot;&gt;http://www.kiwilinux.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripción:&lt;/b&gt; Kiwi Linux és un derviat de l&apos;Ubuntu fet principalment per a usuaris de parla romanesa, hongaresa i anglesa.&lt;br/&gt;&lt;b&gt;Notes de la instal·lació:&lt;/b&gt; l suport d&apos;instal·lació ha d&apos;estar buit i tenir format abans d&apos;instal·lar-lo.</translation>
    </message>
    <message>
        <location filename="distrovercust.cpp" line="69"/>
        <source>&lt;img src=&quot;:/gnewsense.png&quot; /&gt;&lt;br/&gt;&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.gnewsense.org/&quot;&gt;http://www.gnewsense.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; gNewSense is a high-quality GNU/Linux distribution that extends and improves Ubuntu to create a completely free operating system without any binary blobs or package trees that contain proprietary software.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; Make sure install media is empty and formatted before proceeding with install.</source>
        <translation>&lt;img src=&quot;:/gnewsense.png&quot; /&gt;&lt;br/&gt;&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://www.gnewsense.org/&quot;&gt;http://www.gnewsense.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; gNewSense és una distribució GNU/Linux que amplia i millora l&apos;Ubuntu per crear un sistema operatiu totalment lliure sense «blobs» binaris o arbres de paquets que contenen codi propietari.&lt;br/&gt;&lt;b&gt;Notes de la instal·lació:&lt;/b&gt; El suport d&apos;instal·lació ha d&apos;estar buit i tenir format abans d&apos;instal·lar-lo.</translation>
    </message>
    <message>
        <location filename="distrovercust.cpp" line="77"/>
        <source>&lt;img src=&quot;:/nimblex.png&quot; /&gt;&lt;br/&gt;&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.nimblex.net/&quot;&gt;http://www.nimblex.net&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; NimbleX is a small, versatile Slackware-based distribution. It is built using the linux-live scripts, and features the KDE desktop. It can be booted from CD or flash memory (USB pens or MP3 players), and can easily be customized and extended.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; NimbleX boots in Live mode.</source>
        <translation>&lt;img src=&quot;:/nimblex.png&quot; /&gt;&lt;br/&gt;&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://www.nimblex.net/&quot;&gt;http://www.nimblex.net&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; NimbleX és una distribució petita i versàtil basada en Slackware. Està contruïda utilitzant els sripts linux-live i té un escriptori KDE. Es pot arrencar des d&apos;un CD o d&apos;una memòria flash (dispositius USB o reproductors MP3) i es pot personalitzar i ampliar fàcilment. &lt;br/&gt;&lt;b&gt;Notes de la instal·lació:&lt;/b&gt; NimbleX arrenca en mode autònom.</translation>
    </message>
    <message>
        <location filename="distrovercust.cpp" line="85"/>
        <source>&lt;img src=&quot;:/slitaz.png&quot; /&gt;&lt;br/&gt;&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.slitaz.org/en/&quot;&gt;http://www.slitaz.org/en&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; SliTaz is a lightweight, desktop-oriented micro distribution.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version loads the entire system into RAM and boots from memory, so installation is not required but optional. This installer is based on &lt;a href=&quot;http://unetbootin.sourceforge.net/&quot;&gt;UNetbootin&lt;/a&gt;.</source>
        <translation>&lt;img src=&quot;:/slitaz.png&quot; /&gt;&lt;br/&gt;&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://www.slitaz.org/en/&quot;&gt;http://www.slitaz.org/en&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; SliTaz és una microdistribució lleugera orientada a l&apos;ordinador d&apos;escriptori.&lt;br/&gt;&lt;b&gt;Notes de la instal·lació:&lt;/b&gt; La versió autònoma carrega tot el sistema a la RAM i arrenca des de la memòria, per tant la instal·lació no és necessària sinó opcional. Aquest instal·lador està basat en&lt;a href=&quot;http://unetbootin.sourceforge.net/&quot;&gt;UNetbootin&lt;/a&gt;.</translation>
    </message>
    <message>
        <location filename="distrovercust.cpp" line="93"/>
        <source>&lt;img src=&quot;:/xpud.png&quot; /&gt;&lt;br/&gt;&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.xpud.org/&quot;&gt;http://www.xpud.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; xPUD is a lightweight distribution featuring a simple kiosk-like interface with a web browser and media player.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version loads the entire system into RAM and boots from memory.</source>
        <translation>&lt;img src=&quot;:/xpud.png&quot; /&gt;&lt;b&gt;Pàgina web:&lt;/b&gt; &lt;a href=&quot;http://www.xpud.org/&quot;&gt;http://www.xpud.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descripció:&lt;/b&gt; xPUD és una distribució lleugera amb una interfície senzilla tipus quiosc, amb un navegador i un reproductor multimèdia.&lt;br/&gt;&lt;b&gt;Notes de la instal·lació:&lt;/b&gt; La versió autònoma carrega tot el sistema a la RAM i arrenca des de la memòria.</translation>
    </message>
</context>
<context>
    <name>unetbootinui</name>
    <message>
        <location filename="unetbootin.ui" line="20"/>
        <source>Unetbootin</source>
        <translation>Unetbootin</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="44"/>
        <location filename="unetbootin.ui" line="65"/>
        <source>Select from a list of supported distributions</source>
        <translation>Seleccioneu de la llista de distribucions que es dóna suport</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="47"/>
        <source>&amp;Distribution</source>
        <translation>&amp;Distribució</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="131"/>
        <source>Specify a disk image file to load</source>
        <translation>Indiqueu el fitxer d&apos;imatge de disc que s&apos;ha de carregar</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="134"/>
        <source>Disk&amp;image</source>
        <translation>&amp;Imatge de disc</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="147"/>
        <source>Manually specify a kernel and initrd to load</source>
        <translation>Indiqueu manualment un nucli i un initrd per carregar</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="150"/>
        <source>&amp;Custom</source>
        <translation>&amp;Personalitzat</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="414"/>
        <location filename="unetbootin.ui" line="430"/>
        <source>Space to reserve for user files which are preserved across reboots. Works only for LiveUSBs for Ubuntu and derivatives. If value exceeds drive capacity, the maximum space available will be used.</source>
        <translation>Espai reservat per evitar la pèrdua de fitxers en reiniciar. Funciona només a Ubuntu i derivats en mode «LiveUSB». Si el valor excedeix de la capacitat real de la unitat, llavors s&apos;emprarà l&apos;espai màxim d&apos;emmagatzematge disponible.</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="417"/>
        <source>Space used to preserve files across reboots (Ubuntu only):</source>
        <translation>Espai reservat per evitar la pèrdua de fitxers en reiniciar (només Ubuntu):</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="440"/>
        <source>MB</source>
        <translation>MB</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="503"/>
        <source>OK</source>
        <translation>D&apos;acord</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="506"/>
        <source>Return</source>
        <translation>Torna</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="513"/>
        <source>Cancel</source>
        <translation>Cancel·la</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="516"/>
        <source>Esc</source>
        <translation>Esc</translation>
    </message>
    <message>
        <source>Reboot Now</source>
        <translation type="vanished">Reinicia ara</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="561"/>
        <source>Exit</source>
        <translation>Surt</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="653"/>
        <source>1. Downloading Files</source>
        <translation>1. Descàrrega dels fitxers</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="660"/>
        <source>2. Extracting and Copying Files</source>
        <translation>2. Extracció i còpia dels fitxers</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="667"/>
        <source>3. Installing Bootloader</source>
        <translation>3. Instal·lació del carregador de l&apos;arrencada</translation>
    </message>
    <message>
        <source>4. Installation Complete, Reboot</source>
        <translation type="vanished">4. S&apos;ha acabat la instal·lació, torneu a arrencar</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="477"/>
        <location filename="unetbootin.ui" line="496"/>
        <source>Select the target drive to install to</source>
        <translation>Seleccioneu la unitat d&apos;instal·lació</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="480"/>
        <source>Dri&amp;ve:</source>
        <translation>&amp;Unitat:</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="451"/>
        <location filename="unetbootin.ui" line="470"/>
        <source>Select the installation target type</source>
        <translation>Seleccioneu el tipus d&apos;objectiu de la instal·lació</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="454"/>
        <source>&amp;Type:</source>
        <translation>&amp;Tipus:</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="81"/>
        <source>Select the distribution version</source>
        <translation>Seleccioneu la versió de la distribució</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="347"/>
        <source>Select disk image file</source>
        <translation>Seleccioneu el fitxer d&apos;imatge de disc</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="286"/>
        <location filename="unetbootin.ui" line="350"/>
        <location filename="unetbootin.ui" line="375"/>
        <location filename="unetbootin.ui" line="400"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="188"/>
        <source>Select the disk image type</source>
        <translation>Seleccioneu el tipus d&apos;imatge de disc</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="243"/>
        <source>Specify a floppy/hard disk image, or CD image (ISO) file to load</source>
        <translation>Indiqueu la imatge de disquet/disc dur, o el fitxer d&apos;imatge de CD (ISO) que es carregarà</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="207"/>
        <location filename="unetbootin.ui" line="258"/>
        <source>Specify a kernel file to load</source>
        <translation>Indiqueu el fitxer del nucli que es carregarà</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="283"/>
        <source>Select kernel file</source>
        <translation>Seleccioneu el fitxer del nucli</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="293"/>
        <location filename="unetbootin.ui" line="312"/>
        <source>Specify an initrd file to load</source>
        <translation>Indiqueu el fitxer initrd que es carregarà</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="372"/>
        <source>Select initrd file</source>
        <translation>Seleccioneu el fitxer initrd</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="397"/>
        <source>Select syslinux.cfg or isolinux.cfg file</source>
        <translation>Seleccioneu el fitxer syslinux.cfg o isolinux.cfg</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="226"/>
        <location filename="unetbootin.ui" line="321"/>
        <source>Specify parameters and options to pass to the kernel</source>
        <translation>Indiqueu els paràmetres i opcions que es passaran al nucli</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="210"/>
        <source>&amp;Kernel:</source>
        <translation>&amp;Nucli:</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="296"/>
        <source>Init&amp;rd:</source>
        <translation>Init&amp;rd:</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="229"/>
        <source>&amp;Options:</source>
        <translation>&amp;Opcions:</translation>
    </message>
</context>
<context>
    <name>uninstaller</name>
    <message>
        <location filename="main.cpp" line="156"/>
        <source>Uninstallation Complete</source>
        <translation>Desinstal·lació completa</translation>
    </message>
    <message>
        <location filename="main.cpp" line="157"/>
        <source>%1 has been uninstalled.</source>
        <translation>S&apos;ha desinstal·lat %1.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="322"/>
        <source>Must run as root</source>
        <translation>S&apos;ha d&apos;executar com a usuari primari</translation>
    </message>
    <message>
        <location filename="main.cpp" line="324"/>
        <source>%2 must be run as root. Close it, and re-run using either:&lt;br/&gt;&lt;b&gt;sudo %1&lt;/b&gt;&lt;br/&gt;or:&lt;br/&gt;&lt;b&gt;su - -c &apos;%1&apos;&lt;/b&gt;</source>
        <translation>%2 s&apos;ha d&apos;executar com a usuari primari. Tanqueu i torneu a executar-lo utilitzant &lt;br/&gt;&lt;b&gt;sudo %1&lt;/b&gt;&lt;br/&gt;o bé&lt;br/&gt;&lt;b&gt;su - -c &apos;%1&apos;&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="main.cpp" line="361"/>
        <source>%1 Uninstaller</source>
        <translation>Desinstal·lador de l&apos;%1</translation>
    </message>
    <message>
        <location filename="main.cpp" line="362"/>
        <source>%1 is currently installed. Remove the existing version?</source>
        <translation>%1 ja està instal·lat. Voleu suprimir la versió actual?</translation>
    </message>
</context>
</TS>
