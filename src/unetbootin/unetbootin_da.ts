<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="da_DK">
<context>
    <name>QObject</name>
    <message>
        <location filename="main.cpp" line="266"/>
        <source>LeftToRight</source>
        <translation>Venstre mod Højre</translation>
    </message>
</context>
<context>
    <name>customver</name>
    <message>
        <location filename="customdistselect.cpp" line="10"/>
        <location filename="customdistrolst.cpp" line="10"/>
        <source>iscustomver-no</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="12"/>
        <location filename="customdistselect.cpp" line="13"/>
        <source>Custom UNetbootin Build</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="15"/>
        <location filename="customdistrolst.cpp" line="26"/>
        <source>Distro Name 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="15"/>
        <source>default-distro1version-1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="16"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.distro1site.org&quot;&gt;http://www.distro1site.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; This distro 1 is unique and great in these ways.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; You will have to do this to install this distribution 1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>distro1version-1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-1_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-1_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-1_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-2_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-2_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-2_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-3_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-3_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-3_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-4_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-4_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-4_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-5_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-5_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-5_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-6_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-6_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-6_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-7_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-7_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-7_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-8_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-8_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-8_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-9_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-9_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="19"/>
        <source>someotherversion1-9_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="20"/>
        <location filename="customdistselect.cpp" line="22"/>
        <location filename="customdistrolst.cpp" line="39"/>
        <source>Distro Name 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="22"/>
        <source>default-distro2version-1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="23"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.distro2site.org&quot;&gt;http://www.distro2site.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; This distro 2 is unique and great in these ways.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; You will have to do this to install this distribution 2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>distro2version-1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-1_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-1_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-1_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-2_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-2_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-2_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-3_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-3_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-3_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-4_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-4_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-4_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-5_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-5_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-5_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-6_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-6_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-6_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-7_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-7_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-7_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-8_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-8_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-8_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-9_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-9_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="26"/>
        <source>someotherversion2-9_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="28"/>
        <location filename="customdistselect.cpp" line="30"/>
        <location filename="customdistrolst.cpp" line="52"/>
        <source>Distro Name 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="30"/>
        <source>default-distro3version-1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="31"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.distro3site.org&quot;&gt;http://www.distro3site.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; This distro 3 is unique and great in these ways.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; You will have to do this to install this distribution 3.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>distro3version-1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-1_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-1_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-1_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-2_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-2_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-2_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-3_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-3_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-3_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-4_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-4_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-4_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-5_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-5_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-5_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-6_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-6_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-6_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-7_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-7_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-7_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-8_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-8_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-8_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-9_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-9_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="34"/>
        <source>someotherversion3-9_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="36"/>
        <location filename="customdistselect.cpp" line="38"/>
        <location filename="customdistrolst.cpp" line="65"/>
        <source>Distro Name 4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="38"/>
        <source>default-distro4version-1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="39"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.distro4site.org&quot;&gt;http://www.distro4site.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; This distro 4 is unique and great in these ways.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; You will have to do this to install this distribution 4.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>distro4version-1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-1_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-1_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-1_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-2_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-2_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-2_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-3_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-3_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-3_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-4_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-4_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-4_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-5_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-5_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-5_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-6_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-6_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-6_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-7_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-7_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-7_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-8_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-8_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-8_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-9_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-9_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="42"/>
        <source>someotherversion4-9_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="44"/>
        <location filename="customdistselect.cpp" line="46"/>
        <location filename="customdistrolst.cpp" line="78"/>
        <source>Distro Name 5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="46"/>
        <source>default-distro5version-1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="47"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.distro5site.org&quot;&gt;http://www.distro5site.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; This distro 5 is unique and great in these ways.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; You will have to do this to install this distribution 5.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>distro5version-1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-1_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-1_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-1_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-2_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-2_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-2_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-3_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-3_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-3_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-4_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-4_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-4_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-5_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-5_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-5_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-6_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-6_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-6_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-7_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-7_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-7_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-8_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-8_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-8_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-9_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-9_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="50"/>
        <source>someotherversion5-9_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="52"/>
        <location filename="customdistselect.cpp" line="54"/>
        <location filename="customdistrolst.cpp" line="91"/>
        <source>Distro Name 6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="54"/>
        <source>default-distro6version-1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="55"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.distro6site.org&quot;&gt;http://www.distro6site.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; This distro 6 is unique and great in these ways.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; You will have to do this to install this distribution 6.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>distro6version-1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-1_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-1_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-1_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-2_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-2_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-2_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-3_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-3_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-3_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-4_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-4_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-4_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-5_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-5_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-5_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-6_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-6_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-6_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-7_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-7_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-7_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-8_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-8_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-8_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-9_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-9_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="58"/>
        <source>someotherversion6-9_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="60"/>
        <location filename="customdistselect.cpp" line="62"/>
        <location filename="customdistrolst.cpp" line="104"/>
        <source>Distro Name 7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="62"/>
        <source>default-distro7version-1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="63"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.distro7site.org&quot;&gt;http://www.distro7site.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; This distro 7 is unique and great in these ways.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; You will have to do this to install this distribution 7.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>distro7version-1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-1_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-1_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-1_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-2_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-2_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-2_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-3_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-3_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-3_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-4_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-4_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-4_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-5_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-5_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-5_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-6_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-6_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-6_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-7_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-7_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-7_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-8_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-8_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-8_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-9_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-9_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="66"/>
        <source>someotherversion7-9_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="68"/>
        <location filename="customdistselect.cpp" line="70"/>
        <location filename="customdistrolst.cpp" line="117"/>
        <source>Distro Name 8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="70"/>
        <source>default-distro8version-1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="71"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.distro8site.org&quot;&gt;http://www.distro8site.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; This distro 8 is unique and great in these ways.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; You will have to do this to install this distribution 8.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>distro8version-1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-1_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-1_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-1_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-2_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-2_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-2_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-3_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-3_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-3_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-4_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-4_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-4_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-5_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-5_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-5_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-6_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-6_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-6_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-7_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-7_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-7_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-8_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-8_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-8_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-9_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-9_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="74"/>
        <source>someotherversion8-9_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="76"/>
        <location filename="customdistselect.cpp" line="78"/>
        <location filename="customdistrolst.cpp" line="130"/>
        <source>Distro Name 9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="78"/>
        <source>default-distro9version-1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="79"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.distro9site.org&quot;&gt;http://www.distro9site.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; This distro 9 is unique and great in these ways.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; You will have to do this to install this distribution 9.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>distro9version-1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-1_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-1_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-1_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-2_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-2_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-2_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-3_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-3_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-3_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-4_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-4_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-4_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-5_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-5_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-5_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-6_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-6_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-6_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-7_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-7_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-7_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-8_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-8_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-8_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-9_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-9_Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistselect.cpp" line="82"/>
        <source>someotherversion9-9_Live_x64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistrolst.cpp" line="14"/>
        <location filename="customdistrolst.cpp" line="15"/>
        <source>amd64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistrolst.cpp" line="21"/>
        <location filename="customdistrolst.cpp" line="22"/>
        <source>i386</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistrolst.cpp" line="28"/>
        <source>distro1format-iso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistrolst.cpp" line="31"/>
        <location filename="customdistrolst.cpp" line="35"/>
        <source>http://distro1site.org/distro-release-%1/distro-architecture%2.iso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistrolst.cpp" line="41"/>
        <source>distro2format-iso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistrolst.cpp" line="44"/>
        <location filename="customdistrolst.cpp" line="48"/>
        <source>http://distro2site.org/distro-release-%1/distro-architecture%2.iso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistrolst.cpp" line="54"/>
        <source>distro3format-iso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistrolst.cpp" line="57"/>
        <location filename="customdistrolst.cpp" line="61"/>
        <source>http://distro3site.org/distro-release-%1/distro-architecture%2.iso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistrolst.cpp" line="67"/>
        <source>distro4format-iso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistrolst.cpp" line="70"/>
        <location filename="customdistrolst.cpp" line="74"/>
        <source>http://distro4site.org/distro-release-%1/distro-architecture%2.iso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistrolst.cpp" line="80"/>
        <source>distro5format-iso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistrolst.cpp" line="83"/>
        <location filename="customdistrolst.cpp" line="87"/>
        <source>http://distro5site.org/distro-release-%1/distro-architecture%2.iso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistrolst.cpp" line="93"/>
        <source>distro6format-iso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistrolst.cpp" line="96"/>
        <location filename="customdistrolst.cpp" line="100"/>
        <source>http://distro6site.org/distro-release-%1/distro-architecture%2.iso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistrolst.cpp" line="106"/>
        <source>distro7format-iso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistrolst.cpp" line="109"/>
        <location filename="customdistrolst.cpp" line="113"/>
        <source>http://distro7site.org/distro-release-%1/distro-architecture%2.iso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistrolst.cpp" line="119"/>
        <source>distro8format-iso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistrolst.cpp" line="122"/>
        <location filename="customdistrolst.cpp" line="126"/>
        <source>http://distro8site.org/distro-release-%1/distro-architecture%2.iso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistrolst.cpp" line="132"/>
        <source>distro9format-iso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customdistrolst.cpp" line="135"/>
        <location filename="customdistrolst.cpp" line="139"/>
        <source>http://distro9site.org/distro-release-%1/distro-architecture%2.iso</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>unetbootin</name>
    <message>
        <location filename="unetbootin.cpp" line="218"/>
        <location filename="unetbootin.cpp" line="319"/>
        <location filename="unetbootin.cpp" line="320"/>
        <location filename="unetbootin.cpp" line="389"/>
        <location filename="unetbootin.cpp" line="563"/>
        <location filename="unetbootin.cpp" line="3497"/>
        <location filename="unetbootin.cpp" line="3510"/>
        <location filename="unetbootin.cpp" line="3692"/>
        <location filename="unetbootin.cpp" line="4354"/>
        <source>Hard Disk</source>
        <translation>Harddisk</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="220"/>
        <location filename="unetbootin.cpp" line="316"/>
        <location filename="unetbootin.cpp" line="317"/>
        <location filename="unetbootin.cpp" line="391"/>
        <location filename="unetbootin.cpp" line="567"/>
        <location filename="unetbootin.cpp" line="749"/>
        <location filename="unetbootin.cpp" line="769"/>
        <location filename="unetbootin.cpp" line="1018"/>
        <location filename="unetbootin.cpp" line="1593"/>
        <location filename="unetbootin.cpp" line="1680"/>
        <location filename="unetbootin.cpp" line="2665"/>
        <location filename="unetbootin.cpp" line="2688"/>
        <location filename="unetbootin.cpp" line="3501"/>
        <location filename="unetbootin.cpp" line="3527"/>
        <location filename="unetbootin.cpp" line="3696"/>
        <location filename="unetbootin.cpp" line="4020"/>
        <location filename="unetbootin.cpp" line="4358"/>
        <source>USB Drive</source>
        <translation>USB-drev</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="221"/>
        <location filename="unetbootin.cpp" line="238"/>
        <location filename="unetbootin.cpp" line="239"/>
        <location filename="unetbootin.cpp" line="355"/>
        <location filename="unetbootin.cpp" line="681"/>
        <location filename="unetbootin.cpp" line="688"/>
        <location filename="unetbootin.cpp" line="689"/>
        <location filename="unetbootin.cpp" line="3586"/>
        <source>ISO</source>
        <translation>ISO</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="222"/>
        <location filename="unetbootin.cpp" line="234"/>
        <location filename="unetbootin.cpp" line="235"/>
        <location filename="unetbootin.cpp" line="361"/>
        <location filename="unetbootin.cpp" line="681"/>
        <location filename="unetbootin.cpp" line="693"/>
        <location filename="unetbootin.cpp" line="694"/>
        <location filename="unetbootin.cpp" line="3578"/>
        <source>Floppy</source>
        <translation>Diskette</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="255"/>
        <location filename="unetbootin.cpp" line="261"/>
        <location filename="unetbootin.cpp" line="265"/>
        <location filename="unetbootin.cpp" line="269"/>
        <location filename="unetbootin.cpp" line="273"/>
        <location filename="unetbootin.cpp" line="279"/>
        <location filename="unetbootin.cpp" line="307"/>
        <source>either</source>
        <translation>enten</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="286"/>
        <source>LiveUSB persistence</source>
        <translation>LiveUSB vedholdenhed</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="301"/>
        <source>FAT32-formatted USB drive</source>
        <translation>FAT32-formateret USB drev</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="305"/>
        <source>EXT2-formatted USB drive</source>
        <translation>EXT2-formateret USB drev</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="681"/>
        <source>Open Disk Image File</source>
        <translation>Åben Disk Billede Fil</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="681"/>
        <source>All Files</source>
        <translation>Alle filer</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="703"/>
        <location filename="unetbootin.cpp" line="715"/>
        <location filename="unetbootin.cpp" line="727"/>
        <source>All Files (*)</source>
        <translation>Alle Filer (*)</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="703"/>
        <source>Open Kernel File</source>
        <translation>Åben Kernel Fil</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="715"/>
        <source>Open Initrd File</source>
        <translation>Åben Initrd Fil</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="727"/>
        <source>Open Bootloader Config File</source>
        <translation>Åben Bootloaderens Konfigurationsfil</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="753"/>
        <source>Insert a USB flash drive</source>
        <translation>Insæt et USB flash drev</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="754"/>
        <source>No USB flash drives were found. If you have already inserted a USB drive, try reformatting it as FAT32.</source>
        <translation>Der blev ikke fundet noget USB flash drev. Hvis du allerede har indsat et USB drev, så prøv at genformatere det i FAT32 filsystemet.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="773"/>
        <source>%1 not mounted</source>
        <translation>%1 ikke monteret</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="774"/>
        <source>You must first mount the USB drive %1 to a mountpoint. Most distributions will do this automatically after you remove and reinsert the USB drive.</source>
        <translation>Du skal først montere USB drevet %1 til et monteringspunkt. De fleste distributioner vil gøre dette automatisk efter at du har fjernet og genindsat USB drevet.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="789"/>
        <source>Select a distro</source>
        <translation>Vælg en distribution</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="790"/>
        <source>You must select a distribution to load.</source>
        <translation>Du skal vælge en distribution til indlæsning.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="804"/>
        <source>Select a disk image file</source>
        <translation>Vælg en disk billede fil</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="805"/>
        <source>You must select a disk image file to load.</source>
        <translation>Du skal vælge en disk billede fil til indlæsning.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="819"/>
        <source>Select a kernel and/or initrd file</source>
        <translation>Vælg en kernel og/eller initrd fil</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="820"/>
        <source>You must select a kernel and/or initrd file to load.</source>
        <translation>Du skal vælge en kernel og/eller initrd fil til indlæsning.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="834"/>
        <source>Diskimage file not found</source>
        <translation>Disk billede filen blev ikke fundet</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="835"/>
        <source>The specified diskimage file %1 does not exist.</source>
        <translation>Den valgte disk billede fil %1 eksisterer ikke.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="849"/>
        <source>Kernel file not found</source>
        <translation>Kernel fil blev ikke fundet</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="850"/>
        <source>The specified kernel file %1 does not exist.</source>
        <translation>Den valgte kernel fil %1 eksisterer ikke.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="864"/>
        <source>Initrd file not found</source>
        <translation>Initrd fil blev ikke fundet</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="865"/>
        <source>The specified initrd file %1 does not exist.</source>
        <translation>Den valgte initrd fil %1 eksisterer ikke.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="964"/>
        <source>%1 exists, overwrite?</source>
        <translation>%1 findes allerede, overskriv?</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="965"/>
        <source>The file %1 already exists. Press &apos;Yes to All&apos; to overwrite it and not be prompted again, &apos;Yes&apos; to overwrite files on an individual basis, and &apos;No&apos; to retain your existing version. If in doubt, press &apos;Yes to All&apos;.</source>
        <translation>Filen %1 eksisterer allerede. Tryk &apos;Ja til alt&apos; for at overskrive uden at blive spurgt igen, &apos;Ja&apos; for at overskrive filer på individuel basis, og &apos;Nej&apos; for at beholde den eksisterende version. Er du i tvivl, så tryk &apos;Ja til alt&apos;.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="991"/>
        <source>%1 is out of space, abort installation?</source>
        <translation>%1 har ikke mere plads, afbryd installation?</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="992"/>
        <source>The directory %1 is out of space. Press &apos;Yes&apos; to abort installation, &apos;No&apos; to ignore this error and attempt to continue installation, and &apos;No to All&apos; to ignore all out-of-space errors.</source>
        <translation>Biblioteket %1 har ikke mere plads. Tryk &apos;Ja&apos; for at afslutte installationen, &apos;Nej&apos; for at ignorere denne fejl og forsøg at fortsætte installationen, og &apos;Nej til alt&apos; for at ignorere alle pladsmangel fejl.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1086"/>
        <source>Locating kernel file in %1</source>
        <translation>Lokaliserer kernefil i %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1137"/>
        <source>Copying kernel file from %1</source>
        <translation>Kopierer kernefil fra %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1143"/>
        <source>Locating initrd file in %1</source>
        <translation>Lokaliserer initrd-fil i %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1184"/>
        <source>Copying initrd file from %1</source>
        <translation>Kopierer initrd-fil fra %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1190"/>
        <location filename="unetbootin.cpp" line="1270"/>
        <source>Extracting bootloader configuration</source>
        <translation>Udtrækker bootloader-konfiguration</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1499"/>
        <location filename="unetbootin.cpp" line="1525"/>
        <source>&lt;b&gt;Extracting compressed iso:&lt;/b&gt; %1</source>
        <translation>&lt;b&gt;Udpakker komprimeret iso:&lt;/b&gt; %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1779"/>
        <source>Copying file, please wait...</source>
        <translation>Kopierer fil, vent venligst...</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1780"/>
        <location filename="unetbootin.cpp" line="2595"/>
        <source>&lt;b&gt;Source:&lt;/b&gt; &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</source>
        <translation>&lt;b&gt;Kilde:&lt;/b&gt; &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1781"/>
        <location filename="unetbootin.cpp" line="2596"/>
        <source>&lt;b&gt;Destination:&lt;/b&gt; %1</source>
        <translation>&lt;b&gt;Destination:&lt;/b&gt; %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1782"/>
        <source>&lt;b&gt;Copied:&lt;/b&gt; 0 bytes</source>
        <translation>&lt;b&gt;Kopieret:&lt;/b&gt; 0 bytes</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1820"/>
        <source>Extracting files, please wait...</source>
        <translation>Udpakker filer, vent venligst...</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1821"/>
        <source>&lt;b&gt;Archive:&lt;/b&gt; %1</source>
        <translation>&lt;b&gt;Arkiv:&lt;/b&gt; %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1822"/>
        <source>&lt;b&gt;Source:&lt;/b&gt;</source>
        <translation>&lt;b&gt;Kilde:&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1823"/>
        <source>&lt;b&gt;Destination:&lt;/b&gt;</source>
        <translation>&lt;b&gt;Destination:&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1824"/>
        <source>&lt;b&gt;Extracted:&lt;/b&gt; 0 of %1 files</source>
        <translation>&lt;b&gt;Udpakket:&lt;/b&gt; 0 ud af %1 filer</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1827"/>
        <source>&lt;b&gt;Source:&lt;/b&gt; %1 (%2)</source>
        <translation>&lt;b&gt;Kilde:&lt;/b&gt; %1 (%2)</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1828"/>
        <source>&lt;b&gt;Destination:&lt;/b&gt; %1%2</source>
        <translation>&lt;b&gt;Destination:&lt;/b&gt; %1%2</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1829"/>
        <source>&lt;b&gt;Extracted:&lt;/b&gt; %1 of %2 files</source>
        <translation>&lt;b&gt;Udpakket:&lt;/b&gt; %1 ud af %2 filer</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2594"/>
        <source>Downloading files, please wait...</source>
        <translation>Henter filer, vent venligst....</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2597"/>
        <source>&lt;b&gt;Downloaded:&lt;/b&gt; 0 bytes</source>
        <translation>&lt;b&gt;Hentet:&lt;/b&gt; 0 bytes</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2757"/>
        <source>Download of %1 %2 from %3 failed. Please try downloading the ISO file from the website directly and supply it via the diskimage option.</source>
        <translation>Download af %1 %2 fra %3 mislykkedes. Prøv venligst at downloade ISO filen direkte fra hjemmesiden, og tilføj den via &quot;diskimage&quot; valgmuligheden.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2778"/>
        <location filename="unetbootin.cpp" line="2793"/>
        <source>&lt;b&gt;Downloaded:&lt;/b&gt; %1 of %2</source>
        <translation>&lt;b&gt;Hentet:&lt;/b&gt; %1 of %2</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2808"/>
        <source>&lt;b&gt;Copied:&lt;/b&gt; %1 of %2</source>
        <translation>&lt;b&gt;Kopieret:&lt;/b&gt; %1 af %2</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2912"/>
        <source>Searching in &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</source>
        <translation>Søger i &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2916"/>
        <source>%1/%2 matches in &lt;a href=&quot;%3&quot;&gt;%3&lt;/a&gt;</source>
        <translation>%1/%2 fundet i &lt;a href=&quot;%3&quot;&gt;%3&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3170"/>
        <source>%1 not found</source>
        <translation>%1 ikke fundet</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3171"/>
        <source>%1 not found. This is required for %2 install mode.
Install the &quot;%3&quot; package or your distribution&apos;s equivalent.</source>
        <translation>%1 ikke fundet. Denne er krævet for %2 kunne installere.
Installer &quot;%3&quot; pakken eller den tilsvarende for din distribution.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3478"/>
        <source>(Current)</source>
        <translation>(Nuværende)</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3479"/>
        <source>(Done)</source>
        <translation>(Færdig)</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3771"/>
        <source>Configuring grub2 on %1</source>
        <translation>Konfigurerer grub2 på %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3783"/>
        <source>Configuring grldr on %1</source>
        <translation>Konfigurerer grldr på %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3811"/>
        <source>Configuring grub on %1</source>
        <translation>Konfigurerer grub på %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4109"/>
        <source>Installing syslinux to %1</source>
        <translation>Installerer syslinux på %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4144"/>
        <source>Installing extlinux to %1</source>
        <translation>Installerer extlinux på %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4306"/>
        <source>Syncing filesystems</source>
        <translation>Synkroniserer filsystemer</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4311"/>
        <source>Setting up persistence</source>
        <translation>Opsætning af vedholdenhed</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4356"/>
        <source>After rebooting, select the </source>
        <translation>Efter genstart, vælg </translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4361"/>
        <source>After rebooting, select the USB boot option in the BIOS boot menu.%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>After rebooting, select the USB boot option in the BIOS boot menu.%1
Reboot now?</source>
        <translation type="vanished">Efter genstart, vælg USB opstarts muligheden i BIOS boot menuen.%1
Genstart nu?</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4364"/>
        <source>The created USB device will not boot off a Mac. Insert it into a PC, and select the USB boot option in the BIOS boot menu.%1</source>
        <translation>Den oprettede USB enhed vil ikke kunne starte op fra en Mac. Indsæt den i en PC, og vælg USB opstartsmulighed i BIOS opstartsmenuen.%1</translation>
    </message>
    <message>
        <location filename="distrolst.cpp" line="50"/>
        <source>
*IMPORTANT* Before rebooting, place an Ubuntu alternate (not desktop) install iso file on the root directory of your hard drive or USB drive. These can be obtained from cdimage.ubuntu.com</source>
        <translation>
*VIGTIGT* Før du genstarter, bedes du placere en Ubuntu alternate (ikke desktop) installations-iso-fil i rodkataloget på din harddisk eller USB drev. En sådan kan hentes fra cdimage.ubuntu.com</translation>
    </message>
    <message>
        <location filename="distrolst.cpp" line="264"/>
        <source>
*IMPORTANT* After rebooting, ignore any error messages and select back if prompted for a CD, then go to the main menu, select the &apos;Start Installation&apos; option, choose &apos;Network&apos; as the source, choose &apos;HTTP&apos; as the protocol, enter &apos;mirrors.kernel.org&apos; when prompted for a server, and enter &apos;/centos/%1/os/%2&apos; when asked for the folder.</source>
        <translation>
*VIGTIGT* Efter genstart, skal du ignorer eventuelle fejlbeskeder og vælge &apos;tilbage&apos; hvis du bliver spurgt efter en CD. Gå til hovedmenuen, vælg &apos;Start installation&apos;, vælg &apos;Netværk&apos; som kilde, vælg &apos;HTTP&apos; som protokol, indtast &apos;mirrors.kernel.org&apos; når du bliver spurgt efter server, og indtast &apos;/centos/%1/os/%2&apos; når du bliver spurgt efter mappe.</translation>
    </message>
    <message>
        <location filename="distrolst.cpp" line="321"/>
        <source>
*IMPORTANT* Before rebooting, place a Debian install iso file on the root directory of your hard drive or USB drive. These can be obtained from cdimage.debian.org</source>
        <translation>
*VIGTIGT* Før du genstarter, bedes du placere en Debian installations-iso-fil i rodkataloget på din harddisk eller USB drev. En sådan kan hentes fra cdimage.debian.org</translation>
    </message>
    <message>
        <location filename="distrolst.cpp" line="405"/>
        <source>
*IMPORTANT* After rebooting, ignore any error messages and select back if prompted for a CD, then go to the main menu, select the &apos;Start Installation&apos; option, choose &apos;Network&apos; as the source, choose &apos;HTTP&apos; as the protocol, enter &apos;download.fedora.redhat.com&apos; when prompted for a server, and enter &apos;/pub/fedora/linux/development/%1/os&apos; when asked for the folder.</source>
        <translation>
*VIGTIGT* Efter genstart, skal du ignorer eventuelle fejlbeskeder og vælge &apos;tilbage&apos; hvis du bliver spurgt efter en CD. Gå til hovedmenuen, vælg &apos;Start installation&apos;, vælg &apos;Netværk&apos; som kilde, vælg &apos;HTTP&apos; som protokol, indtast &apos;download.fedora.redhat.com&apos; når du bliver spurgt efter server, og indtast &apos;/pub/fedora/linux/development/%1/os&apos; når du bliver spurgt efter mappe.</translation>
    </message>
    <message>
        <location filename="distrolst.cpp" line="411"/>
        <source>
*IMPORTANT* After rebooting, ignore any error messages and select back if prompted for a CD, then go to the main menu, select the &apos;Start Installation&apos; option, choose &apos;Network&apos; as the source, choose &apos;HTTP&apos; as the protocol, enter &apos;download.fedora.redhat.com&apos; when prompted for a server, and enter &apos;/pub/fedora/linux/releases/%1/Fedora/%2/os&apos; when asked for the folder.</source>
        <translation>
*VIGTIGT* Efter genstart, skal du ignorer eventuelle fejlbeskeder og vælge &apos;tilbage&apos; hvis du bliver spurgt efter en CD. Gå til hovedmenuen, vælg &apos;Start installation&apos;, vælg &apos;Netværk&apos; som kilde, vælg &apos;HTTP&apos; som protokol, indtast &apos;download.fedora.redhat.com&apos; når du bliver spurgt efter server, og indtast &apos;/pub/fedora/linux/releases/%1/Fedora/%2/os&apos; når du bliver spurgt efter mappe.</translation>
    </message>
    <message>
        <location filename="distrolst.cpp" line="766"/>
        <source>
*IMPORTANT* After rebooting, ignore any error messages and select back if prompted for a CD, then go to the main menu, select the &apos;Start Installation&apos; option, choose &apos;Network&apos; as the source, choose &apos;HTTP&apos; as the protocol, enter &apos;download.opensuse.org&apos; when prompted for a server, and enter &apos;/factory/repo/oss&apos; when asked for the folder.</source>
        <translation>
*VIGTIGT* Efter genstart, skal du ignorer eventuelle fejlbeskeder og vælge &apos;tilbage&apos; hvis du bliver spurgt efter en CD. Gå til hovedmenuen, vælg &apos;Start installation&apos;, vælg &apos;Netværk&apos; som kilde, vælg &apos;HTTP&apos; som protokol, indtast &apos;download.opensuse.org&apos; når du bliver spurgt efter server, og indtast &apos;/factory/repo/oss&apos; når du bliver spurgt efter mappe.</translation>
    </message>
    <message>
        <location filename="distrolst.cpp" line="772"/>
        <source>
*IMPORTANT* After rebooting, ignore any error messages and select back if prompted for a CD, then go to the main menu, select the &apos;Start Installation&apos; option, choose &apos;Network&apos; as the source, choose &apos;HTTP&apos; as the protocol, enter &apos;download.opensuse.org&apos; when prompted for a server, and enter &apos;/distribution/%1/repo/oss&apos; when asked for the folder.</source>
        <translation>
*VIGTIGT* Efter genstart, skal du ignorer eventuelle fejlbeskeder og vælge &apos;tilbage&apos; hvis du bliver spurgt efter en CD. Gå til hovedmenuen, vælg &apos;Start installation&apos;, vælg &apos;Netværk&apos; som kilde, vælg &apos;HTTP&apos; som protokol, indtast &apos;download.opensuse.org&apos; når du bliver spurgt efter server, og indtast &apos;/distribution/%1/repo/oss&apos; når du bliver spurgt efter mappe.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="19"/>
        <location filename="unetbootin.cpp" line="785"/>
        <source>== Select Distribution ==</source>
        <translation>== Vælg Distribution ==</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="19"/>
        <location filename="distrover.cpp" line="23"/>
        <source>== Select Version ==</source>
        <translation>== Vælg Version ==</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="20"/>
        <source>Welcome to &lt;a href=&quot;http://unetbootin.sourceforge.net/&quot;&gt;UNetbootin&lt;/a&gt;, the Universal Netboot Installer. Usage:&lt;ol&gt;&lt;li&gt;Select a distribution and version to download from the list above, or manually specify files to load below.&lt;/li&gt;&lt;li&gt;Select an installation type, and press OK to begin installing.&lt;/li&gt;&lt;/ol&gt;</source>
        <translation>Velkommen til &lt;a href=&quot;http://unetbootin.sourceforge.net/&quot;&gt;UNetbootin&lt;/a&gt;, den Universelle Netboot Installer. Anvendelse:&lt;ol&gt;&lt;li&gt;Vælg en distribution og version fra listen oven over til download, eller specificer manuelt filer/filen neden under.&lt;/li&gt;&lt;li&gt;Vælg installations type, og tryk OK for at begynde installationen.&lt;/li&gt;&lt;/ol&gt;</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="25"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.archlinux.org/&quot;&gt;http://www.archlinux.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Arch Linux is a lightweight distribution optimized for speed and flexibility.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The default version allows for installation over the internet (FTP).</source>
        <translation>&lt;b&gt;Hjemmeside:&lt;/b&gt; &lt;a href=&quot;http://www.archlinux.org/&quot;&gt;http://www.archlinux.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beskrivelse:&lt;/b&gt; Arch Linux er en letvægts-distribution optimeret for hastighed of fleksibilitet.&lt;br/&gt;&lt;b&gt;Installationsnoter:&lt;/b&gt; Standardudgaven tillader installation over internettet (FTP).</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="30"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.backtrack-linux.org/&quot;&gt;http://www.backtrack-linux.org/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; BackTrack is a distribution focused on network analysis and penetration testing.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; BackTrack is booted and run in live mode; no installation is required to use it.</source>
        <translation>&lt;b&gt;Hjemmeside:&lt;/b&gt; &lt;a href=&quot;http://www.backtrack-linux.org/&quot;&gt;http://www.backtrack-linux.org/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beskrivelse:&lt;/b&gt; BackTrack er en distribution, som fokuserer på netværksanalyse og indtrængningstest.&lt;br/&gt;&lt;b&gt;Installationsnoter:&lt;/b&gt; BackTrack startes op og kører i live mode; ingen installation kræves for at bruge den.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="35"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.centos.org/&quot;&gt;http://www.centos.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; CentOS is a free Red Hat Enterprise Linux clone.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The default version allows for both installation over the internet (FTP), or offline installation using pre-downloaded installation ISO files.</source>
        <translation>&lt;b&gt;Hjemmeside:&lt;/b&gt; &lt;a href=&quot;http://www.centos.org/&quot;&gt;http://www.centos.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beskrivelse:&lt;/b&gt;  CentOS er en gratis Red Hat Enterprise Linux klon.&lt;br/&gt;&lt;b&gt;Installations Noter:&lt;/b&gt; Standard versionen tillader både installation over internettet (FTP), og offline installation ved hjælp af forud hentede installations ISO filer.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="40"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://clonezilla.org/&quot;&gt;http://clonezilla.org/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; CloneZilla is a distribution used for disk backup and imaging.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; CloneZilla is booted and run in live mode; no installation is required to use it.</source>
        <translation>&lt;b&gt;Hjemmeside:&lt;/b&gt; &lt;a href=&quot;http://clonezilla.org/&quot;&gt;http://clonezilla.org/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beskrivelse:&lt;/b&gt; CloneZilla er en distribution beregnet for disk backup and imaging.&lt;br/&gt;&lt;b&gt;Installations Noter:&lt;/b&gt; CloneZilla bootes og køres i live mode; ingen installation nødvendig for brug.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="45"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://damnsmalllinux.org/&quot;&gt;http://damnsmalllinux.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Damn Small Linux is a minimalist distribution designed for older computers.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version loads the entire system into RAM and boots from memory, so installation is not required but optional.</source>
        <translation>&lt;b&gt;Hjemmeside:&lt;/b&gt; &lt;a href=&quot;http://damnsmalllinux.org/&quot;&gt;http://damnsmalllinux.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beskrivelse:&lt;/b&gt;  Damn Small Linux er en minimalistisk distribution designet til ældre computere.&lt;br/&gt;&lt;b&gt;Installations Noter:&lt;/b&gt; Live versionen læser hele systemet ind i RAM og booter fra hukommelsen, så installation er ikke nødvendig, men kan vælges.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="50"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.debian.org/&quot;&gt;http://www.debian.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Debian is a community-developed Linux distribution that supports a wide variety of architectures and offers a large repository of packages.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The NetInstall version allows for installation over FTP. If you would like to use a pre-downloaded install iso, use the HdMedia option, and then place the install iso file on the root directory of your hard drive or USB drive</source>
        <translation>&lt;b&gt;Hjemmeside:&lt;/b&gt; &lt;a href=&quot;http://www.debian.org/&quot;&gt;http://www.debian.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beskrivelse:&lt;/b&gt; Debian er en community-udviklet Linux distribution som understøtter et stort antal arkitekturer og tilbyder en stor samling programpakker.&lt;br/&gt;&lt;b&gt;Installationsnoter:&lt;/b&gt; NetInstall udgaven tillader installation over FTP. Hvis du ønsker at bruge en allerede downloaded installations-iso-fil, skal du benytte HdMedia valgmuligheden, og så placere installations-iso-filen i rodkataloget på din harddisk eller USB-drev.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="56"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.dreamlinux.com.br/&quot;&gt;http://www.dreamlinux.com.br&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Dreamlinux is a user-friendly Debian-based distribution.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched.</source>
        <translation>&lt;b&gt;Hjemmeside:&lt;/b&gt; &lt;a href=&quot;http://www.dreamlinux.com.br/&quot;&gt;http://www.dreamlinux.com.br&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beskrivelse:&lt;/b&gt;  Dreamlinux er en brugervenlig Debian-baseret distribution.&lt;br/&gt;&lt;b&gt;Installation Noter:&lt;/b&gt; Live versionen tillader booting i Live mode, hvorfra man kan vælge at starte installation.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="61"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.freedrweb.com/livecd&quot;&gt;http://www.freedrweb.com/livecd&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Dr.Web AntiVirus is an anti-virus emergency kit to restore a system that broke due to malware.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which malware scans can be launched.</source>
        <translation>&lt;b&gt;Hjemmeside:&lt;/b&gt; &lt;a href=&quot;http://www.freedrweb.com/livecd&quot;&gt;http://www.freedrweb.com/livecd&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beskrivelse:&lt;/b&gt; Dr.Web AntiVirus er et anti-virus emergency kit til gendannelse af systemer der er brudt ned på grund af malware.&lt;br/&gt;&lt;b&gt;Installations Noter:&lt;/b&gt; Live versionen tillader booting i Live mode, hvorfra man kan starte malware scanning.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="66"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.elivecd.org/&quot;&gt;http://www.elivecd.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Elive is a Debian-based distribution featuring the Enlightenment window manager.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched.</source>
        <translation>&lt;b&gt;Forside:&lt;/b&gt; &lt;a href=&quot;http://www.elivecd.org/&quot;&gt;http://www.elivecd.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beskrivelse:&lt;/b&gt; Elive er en Debian-baseret distribution med Enlightenment vindues håndtering.&lt;br/&gt;&lt;b&gt;Installationsnoter:&lt;/b&gt; Live versionen tillader opstart i Live mode, hvor installationsprogrammet eventuelt kan startes.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="71"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://fedoraproject.org/&quot;&gt;http://fedoraproject.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Fedora is a Red Hat sponsored community distribution which showcases the latest cutting-edge free/open-source software.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched. The NetInstall version allows for both installation over the internet (FTP), or offline installation using pre-downloaded installation ISO files.</source>
        <translation>&lt;b&gt;Hjemmeside:&lt;/b&gt; &lt;a href=&quot;http://fedoraproject.org/&quot;&gt;http://fedoraproject.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beskrivelse:&lt;/b&gt; Fedora er en Red Hat sponseret community distribution indeholdende det sidste nye gratis/åbne software.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; Live versionen tillader booting i Live mode, hvorfra man kan vælge at starte installation. NetInstall versionen tillader installation over FTP og offline installation ved hjælp af forud hentede installations ISO filer.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="76"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.freebsd.org/&quot;&gt;http://www.freebsd.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; FreeBSD is a general-purpose Unix-like operating system designed for scalability and performance.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The default version allows for both installation over the internet (FTP), or offline installation using pre-downloaded installation ISO files.</source>
        <translation>&lt;b&gt;Hjemmeside:&lt;/b&gt; &lt;a href=&quot;http://www.freebsd.org/&quot;&gt;http://www.freebsd.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beskrivelse:&lt;/b&gt; FreeBSD er et alsidigt Unix-lignende operativ system designet for skalerbarhed og ydeevne.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; Standard versionen tillader både installation over internettet (FTP), og offline installation ved hjælp af forud hentede installations ISO filer.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="81"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.freedos.org/&quot;&gt;http://www.freedos.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; FreeDOS is a free MS-DOS compatible operating system.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; See the &lt;a href=&quot;http://fd-doc.sourceforge.net/wiki/index.php?n=FdDocEn.FdInstall&quot;&gt;manual&lt;/a&gt; for installation details.</source>
        <translation>&lt;b&gt;Hjemmeside:&lt;/b&gt; &lt;a href=&quot;http://www.freedos.org/&quot;&gt;http://www.freedos.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beskrivelse:&lt;/b&gt; FreeDOS er en fri MS-DOS Kompatibelt operativsystem.&lt;br/&gt;&lt;b&gt;Installations Noter:&lt;/b&gt; Se &lt;a href=&quot;http://fd-doc.sourceforge.net/wiki/index.php?n=FdDocEn.FdInstall&quot;&gt;manual&lt;/a&gt; for installations detaljer.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="86"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://freenas.org/&quot;&gt;http://www.freenas.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; FreeNAS is an embedded open source NAS (Network-Attached Storage) distribution based on FreeBSD.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The LiveCD version creates a RAM drive for FreeNAS, and uses a FAT formatted floppy disk or USB key for saving the configuration file. The embedded version allows installation to hard disk.</source>
        <translation>&lt;b&gt;Forside:&lt;/b&gt; &lt;a href=&quot;http://freenas.org/&quot;&gt;http://www.freenas.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beskrivelse:&lt;/b&gt; FreeNAS er en indlejret open source NAS (Network-Attached Storage) distribution baseret på FreeBSD.&lt;br/&gt;&lt;b&gt;Installationsnoter:&lt;/b&gt; LiveCD versionen opretter et RAM drev til FreeNAS, og bruger en FAT formatteret floppy disk eller USB nøgle til at gemme konfigurationsfilen. Den indlejrede version tillader installation til hard disk.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="91"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://frugalware.org/&quot;&gt;http://frugalware.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Frugalware is a general-purpose Slackware-based distro for advanced users.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The default option allows for both installation over the internet (FTP), or offline installation using pre-downloaded installation ISO files.</source>
        <translation>&lt;b&gt;Hjemmeside:&lt;/b&gt; &lt;a href=&quot;http://frugalware.org/&quot;&gt;http://frugalware.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beskrivelse:&lt;/b&gt; Frugalware er en alsidig Slackware-baseret distribution til avancerede brugere.&lt;br/&gt;&lt;b&gt;Installations Noter:&lt;/b&gt; Standard versionen tillader både installation over internettet (FTP), og offline installation ved hjælp af forud hentede installations ISO filer.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="101"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.geexbox.org/&quot;&gt;http://www.geexbox.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; GeeXboX is an Embedded Linux Media Center Distribution.&lt;br/&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="114"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.gnewsense.org/&quot;&gt;http://www.gnewsense.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; gNewSense is an FSF-endorsed distribution based on Ubuntu with all non-free components removed.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched.</source>
        <translation>&lt;b&gt;Hjemmeside:&lt;/b&gt; &lt;a href=&quot;http://www.gnewsense.org/&quot;&gt;http://www.gnewsense.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beskrivelse:&lt;/b&gt; gNewSense er en Free Software Foundation-godkendt distribution baseret på Ubuntu hvor alle ikke-frie komponenter er fjernet. &lt;br/&gt;&lt;b&gt;Installations Noter:&lt;/b&gt; Live versionen tillader booting i Live mode, hvorfra man kan vælge at starte installation.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="119"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://gujin.sourceforge.net/&quot;&gt;http://gujin.sourceforge.net&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Gujin is a graphical boot manager which can bootstrap various volumes and files.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; Gujin simply boots and runs; no installation is required to use it.</source>
        <translation>&lt;b&gt;Hjemmeside:&lt;/b&gt; &lt;a href=&quot;http://gujin.sourceforge.net/&quot;&gt;http://gujin.sourceforge.net&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beskrivelse:&lt;/b&gt; Gujin er en grafisk boot manager som kan bootstrappe forskellige biblioteker og filer.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; Gujin bootes og køres i live mode; ingen installation nødvendig for brug.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="124"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://ftp.kaspersky.com/devbuilds/RescueDisk/&quot;&gt;http://ftp.kaspersky.com/devbuilds/RescueDisk/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Kaspersky Rescue Disk detects and removes malware from your Windows installation.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which malware scans can be launched.</source>
        <translation>&lt;b&gt;Hjemmeside:&lt;/b&gt; &lt;a href=&quot;http://ftp.kaspersky.com/devbuilds/RescueDisk/&quot;&gt;http://ftp.kaspersky.com/devbuilds/RescueDisk/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beskrivelse:&lt;/b&gt; Kaspersky Rescue Disk finder og fjerner malware fra din  Windows installation.&lt;br/&gt;&lt;b&gt;Installations Noter:&lt;/b&gt; Live versionen tillader booting i Live mode, hvorfra man kan starte scanning for malware.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="129"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.kubuntu.org/&quot;&gt;http://www.kubuntu.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Kubuntu is an official Ubuntu derivative featuring the KDE desktop.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched. The NetInstall version allows for installation over FTP, and can install Kubuntu and other official Ubuntu derivatives. If you would like to use a pre-downloaded alternate (not desktop) install iso, use the HdMedia option, and then place the alternate install iso file on the root directory of your hard drive or USB drive</source>
        <translation></translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="134"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://linuxconsole.org/&quot;&gt;http://linuxconsole.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; LinuxConsole is a desktop distro to play games, easy to install, easy to use and fast to boot .&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The 1.0.2010 is latest 1.0, now available on rolling release (run liveuptate to update modules and kernel).</source>
        <translation></translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="139"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://linuxmint.com/&quot;&gt;http://linuxmint.com&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Linux Mint is a user-friendly Ubuntu-based distribution which includes additional proprietary codecs and other software by default.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched.</source>
        <translation>&lt;b&gt;Hjemmeside:&lt;/b&gt; &lt;a href=&quot;http://linuxmint.com/&quot;&gt;http://linuxmint.com&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beskrivelse:&lt;/b&gt; Linux Mint er en brugervenlig Ubuntu-baseret distribution som inkluderer ekstra proprietære codecs og andet software som standard.&lt;br/&gt;&lt;b&gt;Installations Noter:&lt;/b&gt; Live versionen tillader booting i Live mode, hvorfra man kan vælge at starte installation.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="144"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.lubuntu.net/&quot;&gt;http://www.lubuntu.net&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Lubuntu is an official Ubuntu derivative featuring the LXDE desktop.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched. The NetInstall version allows for installation over FTP, and can install Kubuntu and other official Ubuntu derivatives. If you would like to use a pre-downloaded alternate (not desktop) install iso, use the HdMedia option, and then place the alternate install iso file on the root directory of your hard drive or USB drive</source>
        <translation></translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="149"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.mandriva.com/&quot;&gt;http://www.mandriva.com/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Mandriva is a user-friendly distro formerly known as Mandrake Linux.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched. The NetInstall version allows for installation over the internet (FTP) or via pre-downloaded &lt;a href=&quot;http://www.mandriva.com/en/download&quot;&gt;&quot;Free&quot; iso image files&lt;/a&gt;.</source>
        <translation>&lt;b&gt;Hjemmeside:&lt;/b&gt; &lt;a href=&quot;http://www.mandriva.com/&quot;&gt;http://www.mandriva.com/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beskrivelse:&lt;/b&gt; Mandriva er en brugervenlig distribution tidligere kendt som Mandrake Linux.&lt;br/&gt;&lt;b&gt;Installations Noter:&lt;/b&gt; Live versionen tillader booting i Live mode, hvorfra man kan vælge at starte installation. NetInstall versionen tillader installation over internettet (FTP) eller via forud hentede &lt;a href=&quot;http://www.mandriva.com/en/download&quot;&gt;&quot;Free&quot; ISO filer&lt;/a&gt;.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="154"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.mepis.org/&quot;&gt;http://www.mepis.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; MEPIS is a Debian-based distribution. SimplyMEPIS is a user-friendly version based on KDE, while AntiX is a lightweight version for older computers.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; MEPIS supports booting in Live mode, from which the installer can optionally be launched.</source>
        <translation>&lt;b&gt;Hjemmeside:&lt;/b&gt; &lt;a href=&quot;http://www.mepis.org/&quot;&gt;http://www.mepis.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beskrivelse:&lt;/b&gt; MEPIS er en Debian-baseret distribution. SimplyMEPIS er en brugervenlig version baseret på KDE, mens AntiX er en letvægts version for ældre computere.&lt;br/&gt;&lt;b&gt;Installations Noter:&lt;/b&gt; MEPIS tillader booting i Live mode, hvorfra man kan vælge at starte installation.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="159"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://netbootcd.tuxfamily.org/&quot;&gt;http://netbootcd.tuxfamily.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; NetbootCD is a small boot CD that downloads and boots network-based installers for other distributions.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; NetbootCD boots and runs in live mode.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="169"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.nimblex.net/&quot;&gt;http://www.nimblex.net&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; NimbleX is a small, versatile Slackware-based distribution. It is built using the linux-live scripts, and features the KDE desktop. It can be booted from CD or flash memory (USB pens or MP3 players), and can easily be customized and extended.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; NimbleX boots in Live mode.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="174"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://pogostick.net/~pnh/ntpasswd/&quot;&gt;http://pogostick.net/~pnh/ntpasswd/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; The Offline NT Password and Registry Editor can reset Windows passwords and edit the registry on Windows 2000-Vista.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; NTPasswd is booted and run in live mode; no installation is required to use it.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="179"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.opensuse.org/&quot;&gt;http://www.opensuse.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; openSUSE is a user-friendly Novell sponsored distribution.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The default version allows for both installation over the internet (FTP), or offline installation using pre-downloaded installation ISO files.</source>
        <translation>&lt;b&gt;Hjemmeside:&lt;/b&gt; &lt;a href=&quot;http://www.opensuse.org/&quot;&gt;http://www.opensuse.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beskrivelse:&lt;/b&gt; openSUSE er en brugervenlig Novell sponsoreret distribution.&lt;br/&gt;&lt;b&gt;Installations Noter:&lt;/b&gt; Standard versionen tillader både installation over internettet (FTP), og offline installation ved hjælp af forud hentede installations ISO filer.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="184"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://ophcrack.sourceforge.net/&quot;&gt;http://ophcrack.sourceforge.net&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Ophcrack can crack Windows passwords.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; Ophcrack is booted and run in live mode; no installation is required to use it.</source>
        <translation>&lt;b&gt;Hjemmeside:&lt;/b&gt; &lt;a href=&quot;http://ophcrack.sourceforge.net/&quot;&gt;http://ophcrack.sourceforge.net&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beskrivelse:&lt;/b&gt; Ophcrack kan bryde Windows kodeord.&lt;br/&gt;&lt;b&gt;Installations Noter:&lt;/b&gt; Ophcrack bootes og køres i live mode; ingen installation nødvendig for brug.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="189"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://partedmagic.com/&quot;&gt;http://partedmagic.com&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Parted Magic includes the GParted partition manager and other system utilities which can resize, copy, backup, and manipulate disk partitions.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; Parted Magic is booted and run in live mode; no installation is required to use it.</source>
        <translation>&lt;b&gt;Hjemmeside:&lt;/b&gt; &lt;a href=&quot;http://partedmagic.com/&quot;&gt;http://partedmagic.com&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beskrivelse:&lt;/b&gt; Parted Magic indeholder GParted partition manager og andre værktøjer som kan ændre størrelse, kopiere, tage backup af og manipulere disk partitioner.&lt;br/&gt;&lt;b&gt;Installations Noter:&lt;/b&gt; Parted Magic bootes og køres i live mode; ingen installation nødvendig for brug.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="199"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.puppylinux.com/&quot;&gt;http://www.puppylinux.com&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Puppy Linux is a lightweight distribution designed for older computers.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version loads the entire system into RAM and boots from memory, so installation is not required but optional.</source>
        <translation>&lt;b&gt;Hjemmeside:&lt;/b&gt; &lt;a href=&quot;http://www.puppylinux.com/&quot;&gt;http://www.puppylinux.com&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beskrivelse:&lt;/b&gt; Puppy Linux er en letvægts distribution designet for ældre computere.&lt;br/&gt;&lt;b&gt;Installations Noter:&lt;/b&gt; Live versionen læser hele systemet ind i dine RAM og booter fra hukommelsen, så installation er ikke nødvendig, men kan vælges.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="204"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.sabayonlinux.org/&quot;&gt;http://www.sabayonlinux.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Sabayon Linux is a Gentoo-based Live DVD distribution which features the Entropy binary package manager in addition to the source-based Portage.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched. The LiteMCE edition is 2 GB, while the full edition will need an 8 GB USB drive</source>
        <translation>&lt;b&gt;Hjemmeside:&lt;/b&gt; &lt;a href=&quot;http://www.sabayonlinux.org/&quot;&gt;http://www.sabayonlinux.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beskrivelse:&lt;/b&gt; Sabayon Linux er en Gentoo-baset Live DVD distribution som anvender Entropy binær pakke manageren foruden den kildebaserede Portage.&lt;br/&gt;&lt;b&gt;Installations Noter:&lt;/b&gt; Live versionen tillader booting i Live mode, hvorfra man kan vælge at starte installation. LiteMCE udgaven er på 2 GB, mens den fulde version behøver et 8 GB USB drev.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="209"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://salixos.org&quot;&gt;http://salixos.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Salix is a GNU/Linux distribution based on Slackware (fully compatible) that is simple, fast and easy to use.&lt;br/&gt;Like a bonsai, Salix is small, light &amp; the product of infinite care.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched.&lt;br/&gt;Default root password is &lt;b&gt;live&lt;/b&gt;.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="221"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.slax.org/&quot;&gt;http://www.slax.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Slax is a Slackware-based distribution featuring the KDE desktop.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched.</source>
        <translation>&lt;b&gt;Hjemmeside:&lt;/b&gt; &lt;a href=&quot;http://www.slax.org/&quot;&gt;http://www.slax.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beskrivelse:&lt;/b&gt; Slax er en Slackware-baseret distribution der anvender KDE desktoppen.&lt;br/&gt;&lt;b&gt;Installations Noter:&lt;/b&gt; Live versionen tillader booting i Live mode, hvorfra man kan vælge at starte installation.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="226"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.slitaz.org/en/&quot;&gt;http://www.slitaz.org/en&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; SliTaz is a lightweight, desktop-oriented micro distribution.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version loads the entire system into RAM and boots from memory, so installation is not required but optional.</source>
        <translation>&lt;b&gt;Hjemmeside:&lt;/b&gt; &lt;a href=&quot;http://www.slitaz.org/en/&quot;&gt;http://www.slitaz.org/en&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beskrivelse:&lt;/b&gt; SliTaz er en letvægts, skrivebords-orienteret mikro distribution.&lt;br/&gt;&lt;b&gt;Installations Noter:&lt;/b&gt; Live versionen læser hele systemet ind i dine RAM og booter fra hukommelsen, så installation er ikke nødvendig, men kan vælges.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="231"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://btmgr.sourceforge.net/about.html&quot;&gt;http://btmgr.sourceforge.net/about.html&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Smart Boot Manager is a bootloader which can overcome some boot-related BIOS limitations and bugs.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; SBM simply boots and runs; no installation is required to use it.</source>
        <translation>&lt;b&gt;Hjemmeside:&lt;/b&gt; &lt;a href=&quot;http://btmgr.sourceforge.net/about.html&quot;&gt;http://btmgr.sourceforge.net/about.html&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beskrivelse:&lt;/b&gt; Smart Boot Manager er en bootloader som kan håndtere nogle boot-relaterede BIOS begrænsninger og fejl.&lt;br/&gt;&lt;b&gt;Installations Noter:&lt;/b&gt; SBM bootes og køres i live mode; ingen installation nødvendig for brug.</translation>
    </message>
    <message>
        <location filename="distrovercust.cpp" line="12"/>
        <location filename="distrover.cpp" line="236"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.supergrubdisk.org&quot;&gt;http://www.supergrubdisk.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Super Grub Disk is a bootloader which can perform a variety of MBR and bootloader recovery tasks.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; SGD simply boots and runs; no installation is required to use it.</source>
        <translation>&lt;b&gt;Hjemmeside:&lt;/b&gt; &lt;a href=&quot;http://www.supergrubdisk.org&quot;&gt;http://www.supergrubdisk.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beskrivelse:&lt;/b&gt; Super Grub Disk er en bootloader som kan udføre et bredt udvalg af MBR og bootloader reparations opgaver.&lt;br/&gt;&lt;b&gt;Installations Noter:&lt;/b&gt; SGD simpelthen booter og kører; ingen installation er nødvendig for at bruge den.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="241"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://hacktolive.org/wiki/Super_OS&quot;&gt;http://hacktolive.org/wiki/Super_OS&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Super OS is an unofficial derivative of Ubuntu which includes additional software by default. Requires a 2GB USB drive to install.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="251"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.ubuntu.com/&quot;&gt;http://www.ubuntu.com&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Ubuntu is a user-friendly Debian-based distribution. It is currently the most popular Linux desktop distribution.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched. The NetInstall version allows for installation over FTP, and can install Kubuntu and other official Ubuntu derivatives. If you would like to use a pre-downloaded alternate (not desktop) install iso, use the HdMedia option, and then place the alternate install iso file on the root directory of your hard drive or USB drive</source>
        <translation></translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="256"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.xpud.org/&quot;&gt;http://www.xpud.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; xPUD is a lightweight distribution featuring a simple kiosk-like interface with a web browser and media player.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version loads the entire system into RAM and boots from memory.</source>
        <translation>&lt;b&gt;Hjemmeside:&lt;/b&gt; &lt;a href=&quot;http://www.xpud.org/&quot;&gt;http://www.xpud.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beskrivelse:&lt;/b&gt; xPUD er en letvægts distribution indeholdende en simpel kiosk-lignende brugerflade med webbrowser og mediespiller.&lt;br/&gt;&lt;b&gt;Installations Noter:&lt;/b&gt; Live versionen læser hele systemet ind i dine RAM and booter fra hukommelsen.</translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="261"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.xubuntu.org/&quot;&gt;http://www.xubuntu.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Xubuntu is an official Ubuntu derivative featuring the XFCE desktop.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched. The NetInstall version allows for installation over FTP, and can install Kubuntu and other official Ubuntu derivatives. If you would like to use a pre-downloaded alternate (not desktop) install iso, use the HdMedia option, and then place the alternate install iso file on the root directory of your hard drive or USB drive</source>
        <translation></translation>
    </message>
    <message>
        <location filename="distrover.cpp" line="266"/>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.zenwalk.org/&quot;&gt;http://www.zenwalk.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Zenwalk is a Slackware-based distribution featuring the XFCE desktop.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched.</source>
        <translation>&lt;b&gt;Hjemmeside:&lt;/b&gt; &lt;a href=&quot;http://www.zenwalk.org/&quot;&gt;http://www.zenwalk.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beskrivelse:&lt;/b&gt; Zenwalk er en Slackware-baseret distribution indeholdende XFCE desktoppen.&lt;br/&gt;&lt;b&gt;Installations Noter:&lt;/b&gt; Live versionen tillader booting i Live mode, hvorfra du kan vælge at starte installationen.</translation>
    </message>
    <message>
        <location filename="distrovercust.cpp" line="33"/>
        <source>&lt;img src=&quot;:/eeepclos.png&quot; /&gt;&lt;br/&gt;&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.eeepclinuxos.com/&quot;&gt;http://www.eeepclinuxos.com&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; EeePCLinuxOS is a user-friendly PCLinuxOS based distribution for the EeePC.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; Make sure install media is empty and formatted before proceeding with install.</source>
        <translation>&lt;img src=&quot;:/eeepclos.png&quot; /&gt;&lt;br/&gt;&lt;b&gt;Hjemmeside:&lt;/b&gt; &lt;a href=&quot;http://www.eeepclinuxos.com/&quot;&gt;http://www.eeepclinuxos.com&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beskrivelse:&lt;/b&gt; EeePCLinuxOS er en brugervenlig PCLinuxOS baseret distribution for EeePC.&lt;br/&gt;&lt;b&gt;Installations Noter:&lt;/b&gt; Vær sikker på at installations mediet er tomt og formateret før du fortsætter med installationen.</translation>
    </message>
    <message>
        <location filename="distrovercust.cpp" line="41"/>
        <source>&lt;img src=&quot;:/eeeubuntu.png&quot; style=&quot;float:left;&quot; /&gt;&lt;br/&gt;&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.ubuntu-eee.com/&quot;&gt;http://www.ubuntu-eee.com&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Ubuntu Eee is not only Ubuntu optimized for the Asus Eee PC. It&apos;s an operating system, using the Netbook Remix interface, which favors the best software available instead of open source alternatives (ie. Skype instead of Ekiga).&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; Make sure install media is empty and formatted before proceeding with install.</source>
        <translation>&lt;img src=&quot;:/eeeubuntu.png&quot; style=&quot;float:left;&quot; /&gt;&lt;br/&gt;&lt;b&gt;Hjemmeside:&lt;/b&gt; &lt;a href=&quot;http://www.ubuntu-eee.com/&quot;&gt;http://www.ubuntu-eee.com&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beskrivelse:&lt;/b&gt; Ubuntu Eee er ikke kun Ubuntu optimeret for  Asus Eee PC. Det er et operativsystem der bruger Netbook Remix brugerfladen, som favoriserer det bedste software tilgængeligt i stedet for open source alternatives (f.eks. Skype i stedet for Ekiga).&lt;br/&gt;&lt;b&gt;Installations Noter:&lt;/b&gt;Vær sikker på at installations mediet er tomt og formateret før du fortsætter med installationen.</translation>
    </message>
    <message>
        <location filename="distrovercust.cpp" line="53"/>
        <source>&lt;img src=&quot;:/elive.png&quot; /&gt;&lt;br/&gt;&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.elivecd.org/&quot;&gt;http://www.elivecd.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Elive is a Debian-based distribution featuring the Enlightenment window manager.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="distrovercust.cpp" line="61"/>
        <source>&lt;img src=&quot;:/kiwi_logo_ro.png&quot; /&gt;&lt;br/&gt;&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.kiwilinux.org/&quot;&gt;http://www.kiwilinux.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Kiwi Linux is an Ubuntu derivative primarily made for Romanian, Hungarian and English speaking users.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; Make sure install media is empty and formatted before proceeding with install.</source>
        <translation>&lt;img src=&quot;:/kiwi_logo_ro.png&quot; /&gt;&lt;br/&gt;&lt;b&gt;Hjemmeside:&lt;/b&gt; &lt;a href=&quot;http://www.kiwilinux.org/&quot;&gt;http://www.kiwilinux.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beskrivelse:&lt;/b&gt; Kiwi Linux er en Ubuntu variant primært lavet for Rumænien, Ungarn og Engelsktalende brugere.&lt;br/&gt;&lt;b&gt;Installations Noter:&lt;/b&gt; Vær sikker på at installations mediet er tomt og formateret før du fortsætter med installationen.</translation>
    </message>
    <message>
        <location filename="distrovercust.cpp" line="69"/>
        <source>&lt;img src=&quot;:/gnewsense.png&quot; /&gt;&lt;br/&gt;&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.gnewsense.org/&quot;&gt;http://www.gnewsense.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; gNewSense is a high-quality GNU/Linux distribution that extends and improves Ubuntu to create a completely free operating system without any binary blobs or package trees that contain proprietary software.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; Make sure install media is empty and formatted before proceeding with install.</source>
        <translation>&lt;img src=&quot;:/gnewsense.png&quot; /&gt;&lt;br/&gt;&lt;b&gt;Hjemmeside:&lt;/b&gt; &lt;a href=&quot;http://www.gnewsense.org/&quot;&gt;http://www.gnewsense.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beskrivelse:&lt;/b&gt; gNewSense er en høj-kvalitets GNU/Linux distribution som udvider og forbedrer Ubuntu til et komplet fri operativsystem uden nogen binære &quot;blobs&quot; og uden proprietær software.&lt;br/&gt;&lt;b&gt;Installations Noter:&lt;/b&gt; Vær sikker på at installations mediet er tomt og formateret før du fortsætter med installationen.</translation>
    </message>
    <message>
        <location filename="distrovercust.cpp" line="77"/>
        <source>&lt;img src=&quot;:/nimblex.png&quot; /&gt;&lt;br/&gt;&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.nimblex.net/&quot;&gt;http://www.nimblex.net&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; NimbleX is a small, versatile Slackware-based distribution. It is built using the linux-live scripts, and features the KDE desktop. It can be booted from CD or flash memory (USB pens or MP3 players), and can easily be customized and extended.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; NimbleX boots in Live mode.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="distrovercust.cpp" line="85"/>
        <source>&lt;img src=&quot;:/slitaz.png&quot; /&gt;&lt;br/&gt;&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.slitaz.org/en/&quot;&gt;http://www.slitaz.org/en&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; SliTaz is a lightweight, desktop-oriented micro distribution.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version loads the entire system into RAM and boots from memory, so installation is not required but optional. This installer is based on &lt;a href=&quot;http://unetbootin.sourceforge.net/&quot;&gt;UNetbootin&lt;/a&gt;.</source>
        <translation>&lt;img src=&quot;:/slitaz.png&quot; /&gt;&lt;br/&gt;&lt;b&gt;Hjemmeside:&lt;/b&gt; &lt;a href=&quot;http://www.slitaz.org/en/&quot;&gt;http://www.slitaz.org/en&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beskrivelse:&lt;/b&gt; SliTaz er en letvægts, desktop-orienteret micro distribution.&lt;br/&gt;&lt;b&gt;Installations Noter:&lt;/b&gt; Live versionen læser hele systemet ind i dine RAM og booter fra hukommelsen, så installation er ikke nødvendig, men kan vælges. Denne installer er baseret på &lt;a href=&quot;http://unetbootin.sourceforge.net/&quot;&gt;UNetbootin&lt;/a&gt;.</translation>
    </message>
    <message>
        <location filename="distrovercust.cpp" line="93"/>
        <source>&lt;img src=&quot;:/xpud.png&quot; /&gt;&lt;br/&gt;&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.xpud.org/&quot;&gt;http://www.xpud.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; xPUD is a lightweight distribution featuring a simple kiosk-like interface with a web browser and media player.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version loads the entire system into RAM and boots from memory.</source>
        <translation>&lt;img src=&quot;:/xpud.png&quot; /&gt;&lt;br/&gt;&lt;b&gt;Hjemmeside:&lt;/b&gt; &lt;a href=&quot;http://www.xpud.org/&quot;&gt;http://www.xpud.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beskrivelse:&lt;/b&gt; xPUD er en letvægts distribution indeholdende en simpel kiosk-lignende brugerflade med en webbrowser og medieafspiller.&lt;br/&gt;&lt;b&gt;Installations Noter:&lt;/b&gt; Live versionen læser hele systemet ind i dine RAM og booter fra hukommelsen.</translation>
    </message>
</context>
<context>
    <name>unetbootinui</name>
    <message>
        <location filename="unetbootin.ui" line="20"/>
        <source>Unetbootin</source>
        <translation>Unetbootin</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="44"/>
        <location filename="unetbootin.ui" line="65"/>
        <source>Select from a list of supported distributions</source>
        <translation>Vælg fra en liste af tilgængelige distributioner</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="47"/>
        <source>&amp;Distribution</source>
        <translation>&amp;Distribution</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="131"/>
        <source>Specify a disk image file to load</source>
        <translation>Vælg en disk billede fil til indlæsning</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="134"/>
        <source>Disk&amp;image</source>
        <translation>D&amp;isk billede</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="147"/>
        <source>Manually specify a kernel and initrd to load</source>
        <translation>Vælg en kernel og initrd fil til indlæsning manuelt</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="150"/>
        <source>&amp;Custom</source>
        <translation>&amp;Brugerdefineret</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="414"/>
        <location filename="unetbootin.ui" line="430"/>
        <source>Space to reserve for user files which are preserved across reboots. Works only for LiveUSBs for Ubuntu and derivatives. If value exceeds drive capacity, the maximum space available will be used.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="417"/>
        <source>Space used to preserve files across reboots (Ubuntu only):</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="440"/>
        <source>MB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="503"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="506"/>
        <source>Return</source>
        <translation>Enter</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="513"/>
        <source>Cancel</source>
        <translation>Annullér</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="516"/>
        <source>Esc</source>
        <translation>Esc</translation>
    </message>
    <message>
        <source>Reboot Now</source>
        <translation type="vanished">Genstart nu</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="561"/>
        <source>Exit</source>
        <translation>Afslut</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="653"/>
        <source>1. Downloading Files</source>
        <translation>1. Henter filer</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="660"/>
        <source>2. Extracting and Copying Files</source>
        <translation>2. Udpakker og kopierer filer</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="667"/>
        <source>3. Installing Bootloader</source>
        <translation>3. Installerer bootloader</translation>
    </message>
    <message>
        <source>4. Installation Complete, Reboot</source>
        <translation type="vanished">4. Installation færdig, Genstart</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="477"/>
        <location filename="unetbootin.ui" line="496"/>
        <source>Select the target drive to install to</source>
        <translation>Vælg destinations drev for installationen</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="480"/>
        <source>Dri&amp;ve:</source>
        <translation>Dre&amp;v:</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="451"/>
        <location filename="unetbootin.ui" line="470"/>
        <source>Select the installation target type</source>
        <translation>Vælg hvilket type medie du vil skrive til</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="454"/>
        <source>&amp;Type:</source>
        <translation>&amp;Type:</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="81"/>
        <source>Select the distribution version</source>
        <translation>Vælg distributions version</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="347"/>
        <source>Select disk image file</source>
        <translation>Vælg disk billede fil</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="286"/>
        <location filename="unetbootin.ui" line="350"/>
        <location filename="unetbootin.ui" line="375"/>
        <location filename="unetbootin.ui" line="400"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="188"/>
        <source>Select the disk image type</source>
        <translation>Vælg disk billede type</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="243"/>
        <source>Specify a floppy/hard disk image, or CD image (ISO) file to load</source>
        <translation>Vælg et floppy/hard disk billede, eller CD aftryk (ISO) fil til indlæsning</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="207"/>
        <location filename="unetbootin.ui" line="258"/>
        <source>Specify a kernel file to load</source>
        <translation>Vælg en kernel fil til indlæsning</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="283"/>
        <source>Select kernel file</source>
        <translation>Vælg kernel fil</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="293"/>
        <location filename="unetbootin.ui" line="312"/>
        <source>Specify an initrd file to load</source>
        <translation>Vælg en initrd fil til indlæsning</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="372"/>
        <source>Select initrd file</source>
        <translation>Vælg initrd fil</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="397"/>
        <source>Select syslinux.cfg or isolinux.cfg file</source>
        <translation>Vælg syslinux.cfg eller isolinux.cfg fil</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="226"/>
        <location filename="unetbootin.ui" line="321"/>
        <source>Specify parameters and options to pass to the kernel</source>
        <translation>Specificer parametre og indstillinger for kernel</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="210"/>
        <source>&amp;Kernel:</source>
        <translation>&amp;Kernel:</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="296"/>
        <source>Init&amp;rd:</source>
        <translation>Init&amp;rd:</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="229"/>
        <source>&amp;Options:</source>
        <translation>&amp;Indstillinger</translation>
    </message>
</context>
<context>
    <name>uninstaller</name>
    <message>
        <location filename="main.cpp" line="156"/>
        <source>Uninstallation Complete</source>
        <translation>Afinstallation færdig</translation>
    </message>
    <message>
        <location filename="main.cpp" line="157"/>
        <source>%1 has been uninstalled.</source>
        <translation>%1 er blevet afinstalleret.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="322"/>
        <source>Must run as root</source>
        <translation>Skal køre som root</translation>
    </message>
    <message>
        <location filename="main.cpp" line="324"/>
        <source>%2 must be run as root. Close it, and re-run using either:&lt;br/&gt;&lt;b&gt;sudo %1&lt;/b&gt;&lt;br/&gt;or:&lt;br/&gt;&lt;b&gt;su - -c &apos;%1&apos;&lt;/b&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="main.cpp" line="361"/>
        <source>%1 Uninstaller</source>
        <translation>%1 Afinstallation</translation>
    </message>
    <message>
        <location filename="main.cpp" line="362"/>
        <source>%1 is currently installed. Remove the existing version?</source>
        <translation>%1 er installeret nu. Fjern den eksisterende version?</translation>
    </message>
</context>
</TS>
